eppy==0.5.53
matplotlib==3.3.2
numpy==1.19.5
pandas==1.1.2
psychrochart==0.3.1
pyexcel==0.6.4
pyexcel_ods==0.6.0
pyexcel_xlsx==0.6.0
seaborn==0.11.0
openstudio==3.1.0
pyarrow==2.0.0

requests  # only needed to download datapoints... usually not used

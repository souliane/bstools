# -*- coding: utf-8 -*-
from datetime import datetime


def info(*args, **kwargs):
    print(*args, **kwargs)


debug = warn = error = info  # TODO


def step(*args, **kwargs):
    info("[{}]".format(datetime.now().strftime("%H:%M")), *args, **kwargs)

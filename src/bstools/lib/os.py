# -*- coding: utf-8 -*-

import shutil
import subprocess
from pathlib import Path
from subprocess import CompletedProcess


class OSTools:
    @classmethod
    def execute(cls, cmd: str, shell: bool = False, **kwargs) -> CompletedProcess:
        return subprocess.run(
            cmd if shell else cmd.split(" "), check=True, shell=True, **kwargs
        )

    @classmethod
    def wsl_to_windows(cls, path: Path) -> str:
        return (
            cls.execute(f"wslpath -w {path}", shell=True, capture_output=True)
            .stdout.strip()
            .decode()
            .replace("\\", "/")
        )

    @classmethod
    def create_directory(cls, path: Path):
        print(f"Creating {path}")
        path.mkdir(parents=True, exist_ok=False)

    @classmethod
    def copy(cls, src: Path, dst: Path, silent: bool = False):
        if not silent:
            print(f"Copying {src} -> {dst}")
        shutil.copy(src, dst)

    @classmethod
    def rename(cls, src: Path, dst: Path):
        print(f"Renaming {src} -> {dst}")
        if not dst.parent.exists():
            OSTools.create_directory(dst.parent)
        src.rename(dst)

    @classmethod
    def replace(cls, src: Path, dst: Path):
        if dst.is_dir():
            cls.clean_directory(dst)
        print(f"Replacing {src} -> {dst}")
        src.replace(dst)

    @classmethod
    def clean_directory(cls, path: Path):
        print(f"  Cleaning {path}")
        for child in path.iterdir():
            try:
                child.unlink()
            except IsADirectoryError:
                cls.remove_directory(child)

    @classmethod
    def remove_directory(cls, path: Path):
        print(f"  Removing {path}")
        shutil.rmtree(path)  # also works when not empty

    @classmethod
    def create_or_clean_directory(cls, path: Path):
        if path.exists():
            cls.clean_directory(path)
        else:
            cls.create_directory(path)

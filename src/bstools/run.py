#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from pathlib import Path

from bstools import (
    DatapointsProcessor,
    EnergyplusResultsProcessor,
    HVACTemplateExpander,
    IDFEditor,
    ODSLightingProcessor,
    OpenStudioResultsProcessor,
    ReverberationTimeProcessor,
    Settings,
    SimulationLogs,
    WeatherFilesProcessor,
    Workflow,
    download_datapoints,
)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--search-idd",
        type=str,
        default=None,
        help="Search for a pattern in energyplus IDD file.",
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        default=False,
        help="Also display object types and fields descriptions when searching in IDD.",
    )
    parser.add_argument(
        "--expand-hvac-template",
        action="store_true",
        default=False,
        help="Export a table of HVACTemplates and their expansion objects to ODS file.",
    )
    parser.add_argument(
        "--search-hvac-template",
        type=str,
        default=None,
        help="List HVACTemplates that could expand to given comma-separated objects.",
    )
    parser.add_argument(
        "--diff-hvac-template",
        type=str,
        default=None,
        help="Print the expansion differences between two comma-separated HVACTemplates.",
    )

    parser.add_argument(
        "--edit-model",
        action="store_true",
        default=False,
        help="Edit energyplus and OpenStudio models without simulating.",
    )
    parser.add_argument(
        "--workflow",
        action="store_true",
        default=False,
        help="Run the full simulation workflow for energyplus and OpenStudio.",
    )
    parser.add_argument(
        "--use-existing-results",
        action="store_true",
        default=False,
        help=(
            "Complete the workflow for existing energyplus and OpenStudio "
            "results (when the previous simulation succeeded but the results "
            "could not be copied because a file was locked by another program)."
        ),
    )
    parser.add_argument(
        "--errors",
        action="store_true",
        default=False,
        help="Print energyplus simulations warnings and errors.",
    )
    parser.add_argument(
        "--variant",
        type=str,
        default=None,
        help="Edit / simulate / show errors for this scenario variant only.",
    )

    parser.add_argument(
        "--acoustics",
        action="store_true",
        default=False,
        help="Generate acoustics graphs from custom measurements.",
    )
    parser.add_argument(
        "--lighting",
        action="store_true",
        default=False,
        help="Generate lighting graphs from custom measurements.",
    )

    parser.add_argument(
        "--download-datapoints",
        action="store_true",
        default=False,
        help="Download datapoints from your BMS.",
    )
    parser.add_argument(
        "--datapoints",
        action="store_true",
        default=False,
        help="Generate graphs from downloaded datapoints.",
    )

    parser.add_argument(
        "--energyplus",
        action="store_true",
        default=False,
        help="Generate graphs from energyplus results.",
    )
    parser.add_argument(
        "--openstudio",
        action="store_true",
        default=False,
        help="Generate graphs from the outputs of OpenStudioResults measure.",
    )
    parser.add_argument(
        "--group",
        type=str,
        default=None,
        choices=[
            group.dirname
            for group in Settings.EnergyplusResultsProcessor.simulation_groups
        ],
        help=(
            "Generate from energyplus results or OpenStudioResults measure "
            "for this simulation group only."
        ),
    )

    parser.add_argument(
        "--all-plots",
        action="store_true",
        default=False,
        help="Generate all graphs at once.",
    )

    parser.add_argument(
        "--compare-epw",
        action="store_true",
        default=False,
        help="Plot graphs to compare EPW weather files.",
    )

    parser.add_argument(
        "--overwrite",
        action="store_true",
        default=False,
        help="Overwrite existing data.",
    )
    return parser.parse_args()


def main():
    args = parse_args()
    overwrite = args.overwrite
    simulation_group = (
        [
            group
            for group in Settings.EnergyplusResultsProcessor.simulation_groups
            if group.dirname == args.group
        ][0]
        if args.group
        else None
    )

    if args.search_idd:
        IDFEditor(idf_path=Path("dummy.idf")).search_idd(args.search_idd, args.verbose)

    if any(
        (
            args.expand_hvac_template,
            args.search_hvac_template,
            args.diff_hvac_template,
        )
    ):
        HVACTemplateExpander().run(
            args.search_hvac_template, args.diff_hvac_template, args.overwrite
        )

    if any(
        (
            args.edit_model,
            args.workflow,
        )
    ):
        Workflow().run(
            args.variant,
            overwrite=overwrite,
            skip_simulation=args.edit_model,
            use_existing_results=args.use_existing_results,
        )

    if args.errors:
        SimulationLogs().errors(args.variant)

    if any((args.all_plots, args.acoustics)):
        ReverberationTimeProcessor(overwrite=overwrite).process()

    if any((args.all_plots, args.lighting)):
        ODSLightingProcessor(overwrite=overwrite).process()

    if args.download_datapoints:
        download_datapoints(overwrite=overwrite)

    if any((args.all_plots, args.datapoints)):
        DatapointsProcessor.process_date_ranges(init_kwargs={"overwrite": overwrite})

    if any((args.all_plots, args.energyplus)):
        EnergyplusResultsProcessor.process_groups(
            init_kwargs={"overwrite": overwrite}, group=simulation_group
        )

    if any((args.all_plots, args.openstudio)):
        OpenStudioResultsProcessor.process_groups(
            init_kwargs={"overwrite": overwrite}, group=simulation_group
        )

    if args.compare_epw:
        WeatherFilesProcessor(overwrite=overwrite).process()


if __name__ == "__main__":
    main()

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import math
import os
import re
import statistics
from collections import OrderedDict

import numpy as np
import pandas as pd
import scipy.stats
from pandas.core.frame import DataFrame
from pyexcel import get_sheet

from ..lib.logger import step
from ..plotlib import Dimension, Lineplot, ManyFilesProcessor, Room, ValueRange
from ..settings import Settings

# Austrian ÖNORM B 8115-3, purpose "communication"
TOLERANCE_RANGES = OrderedDict(
    (
        ("63Hz", ValueRange(0.50, 1.20)),
        ("125Hz", ValueRange(0.65, 1.20)),
        ("250Hz", ValueRange(0.80, 1.20)),
        ("500Hz", ValueRange(0.80, 1.20)),
        ("1kHz", ValueRange(0.80, 1.20)),
        ("2kHz", ValueRange(0.80, 1.20)),
        ("4kHz", ValueRange(0.65, 1.20)),
        ("8kHz", ValueRange(0.50, 1.20)),
    )
)


class Nor140MeasurementsProcessorMixin:
    input_dir = NotImplemented
    settings = NotImplemented

    def get_input_filepaths(self, *args, room_key: str, **kwargs):  # @UnusedVariable
        return [
            self.input_dir / path
            for path in os.listdir(self.input_dir)
            if re.search(
                r"^[^~].*({})-SummaryReverbReport.*\.xlsx".format(
                    "|".join(f"{index:04}" for index in self.settings.zones[room_key])
                ),
                path,
            )
        ]

    def parse_input_files(self, filepaths: list, room_key: str):
        data = []
        for dataset, filepath in zip(self.settings.zones[room_key], filepaths):
            step(f"Parsing {filepath}")
            sheet = get_sheet(file_name=str(filepath), sheet_name="Reverberation")
            sheet.filter(
                # this is hardcoded because we always get the same format from Nor140
                column_indices=[0],
                row_indices=list(range(8)) + list(range(32, 41)),
            )
            for row in sheet:
                if not any(row):
                    continue
                frequency, t20, t30 = row
                if frequency in TOLERANCE_RANGES.keys():
                    index = self.frequency_str_to_int(frequency)

                    # to avoid quantitative color mapping when plotting with "hue"
                    dataset = str(dataset)

                    data.append([index, frequency, t20 or None, "T20", dataset])
                    data.append([index, frequency, t30 or None, "T30", dataset])

        df = pd.DataFrame(
            data,
            columns=[
                "index",
                "Frequency [Hz]",
                "Reverberation Time [s]",
                "Measurement",
                "Dataset",
            ],
        )
        # "1kH" < "63Hz", so we sort here with integer indices
        df.sort_values(by=["index", "Dataset"], inplace=True)

        return df


class ReverberationTimeProcessor(Nor140MeasurementsProcessorMixin, ManyFilesProcessor):
    settings = Settings.ReverberationTimeProcessor

    @classmethod
    def frequency_str_to_int(cls, frequency):
        assert " " not in frequency
        return int(
            float(frequency.replace("Hz", "").replace("k", ""))
            * (1000 if "k" in frequency else 1)
        )

    def get_optimal_T60(self, room: Room):
        return (
            # ÖNORM B 8115-3 purpose "Communication" and DIN 18041:2016 purpose "A3"
            (0.32 * math.log10(room.volume) - 0.17)
            if room.volume <= 1000
            # ÖNORM B 8115-3, purpose "Talks" and DIN 18041:2016 purpose "A2"
            else (0.37 * math.log10(room.volume) - 0.14)
        ) + self.settings.optimal_reverberation_time_compensation_offset

    def plot_RT_ratio(self, df: DataFrame, room: Room):
        optimal_T60 = self.get_optimal_T60(room)
        print(f"  Optimal RT60 for {room.name} is {optimal_T60}s")

        data = []
        min_col, max_col, ratio_col = "Tolerance Min", "Tolerance Max", "Ratio"
        for frequency, value_range in TOLERANCE_RANGES.items():
            index = self.frequency_str_to_int(frequency)

            # the ratio is calculated from the mean of the T30 values that have an
            # acceptable zscore
            entries = df[
                (df["Frequency [Hz]"] == frequency)
                & (df["Measurement"] == "T30")
                & (df["Reverberation Time [s]"].notnull())
            ]["Reverberation Time [s]"]
            if len(entries):  # all values are NaN
                entries = entries[np.absolute(scipy.stats.zscore(entries)) <= 1]
                ratio = statistics.mean(entries) / optimal_T60
                data.append([index, frequency, ratio_col, ratio])

            data.append([index, frequency, min_col, value_range.min])
            data.append([index, frequency, max_col, value_range.max])

        df = pd.DataFrame(data, columns=["index", "Frequency [Hz]", "Value", "Ratio"])

        # "1kH" < "63Hz", so we sort here with integer indices
        df.sort_values(by=["index"], inplace=True)

        Lineplot(room.name).plot(
            df=df,
            x="Frequency [Hz]",
            y="Ratio",
            hue="Value",
            # use hue_order to avoid 63Hz to be displayed last when RT is NaN
            hue_order=[min_col, max_col, ratio_col],
            title=f"Reverberation Time / Recommended Value in {room.name}",
            filepath=self.output_dir / f"{room.normalized_name}_RT_ratio.png",
            decorate_graph_kwargs={
                "fill_between": [df["Frequency [Hz]"].unique()]
                + list(
                    zip(
                        *(
                            (range_.min, range_.max)
                            for range_ in TOLERANCE_RANGES.values()
                        )
                    )
                )
            },
        )

    def process_data(self, data: DataFrame, room_key: str):
        room = Room.get_by_key(self.settings.rooms, room_key)
        kwargs = {
            "df": data,
            "x": "Frequency [Hz]",
            "y": "Reverberation Time [s]",
        }
        Lineplot(room.name, figure_size=Dimension(9, 6)).plot(
            hue="Dataset",
            style="Measurement",
            title=f"Reverberation Time in {room.name}",
            filepath=self.output_dir / f"{room.normalized_name}_RT.png",
            **kwargs,
        )
        Lineplot(room.name, figure_size=Dimension(9, 5)).plot(
            hue="Measurement",
            estimator="mean",
            ci="sd",
            title=f"Mean Reverberation Time With Standard Deviation in {room.name}",
            filepath=self.output_dir / f"{room.normalized_name}_RT.png",
            **kwargs,
        )
        self.plot_RT_ratio(data, room)

    def process(self, *args, **kwargs):
        for room_key in self.settings.zones.keys():
            super().process(*args, room_key=room_key, **kwargs)

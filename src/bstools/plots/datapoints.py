#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
from datetime import date
from pathlib import Path

import pandas as pd
import requests

from ..lib.logger import step
from ..lib.tools import get
from ..settings import Settings
from .absence_filter import WeekendsAndHolidays
from .timeseries import TimeSeriesProcessor


def download_datapoints(to_date: str = None, overwrite: bool = False):
    from_date = Settings.DatapointsProcessor.DateRange.year.value[0]
    to_date = to_date or date.today().isoformat()

    step(f"Downloading all datapoints from {from_date} to {to_date}")

    with open(Settings.DatapointsProcessor.description_filename, "r") as fp:
        bms_url = Settings.DatapointsProcessor.building_monitoring_system_url
        for datapoint in fp.readlines():
            datapoint = datapoint.strip()
            print(f"{datapoint}", end="")

            output_path = f"{Settings.DatapointsProcessor.input_dir}/{datapoint}.csv"
            if Path(output_path).exists() and not overwrite:
                print(f" already exists.")
            else:
                print()  # newline because we previously used end=""
                r = requests.get(
                    f"{bms_url}?id=&DataPoints="
                    f"{datapoint}&StartDate={from_date}&EndDate={to_date}&Download=1"
                )
                with open(output_path, "w") as fp_out:
                    fp_out.write(r.text)


class DatapointsProcessor(TimeSeriesProcessor):

    FIX_SUMMER_TIME = True
    absence_filter = WeekendsAndHolidays
    null_dataset_name: str = "Measurements"
    settings = Settings.DatapointsProcessor

    def get_input_filepaths(self) -> list:
        return [Path(filename) for filename in self.zones.values()]

    def _pre_process_data(self, df: pd.DataFrame):
        # Windows values have been written with the "Dataset" information but in the end
        # we want the count of opened windows to be written next to the other quantities
        # (where "Dataset" is NaN).
        df, windows_df = df[df.Dataset.isnull()].copy(), df[df.Dataset.notnull()]

        if not windows_df.size:
            return df
        else:
            assert not self.some_values_exist(df, "windows")
            assert self.all_values_exist(windows_df, "windows")

            windows_df = windows_df.groupby(["DateTime", "Room"]).sum()
            datetimes = windows_df.index.get_level_values(0)
            rooms = windows_df.index.get_level_values(1)

            # Custom "forward fill" implementation to extrapolate the values because they
            # are written at non regular frequencies, sometime each 20 mn, sometimes each
            # hour...
            step("Counting the number of opened windows...")
            windows_axis = self.TimeSeries.get_axis("windows")
            df[windows_axis] = df.apply(
                lambda row: windows_df[
                    (datetimes <= row.DateTime) & (rooms == row.Room)
                ][windows_axis].iloc[-1],
                axis=1,
            )
            return df.sort_values(by=["DateTime", "Room", "Dataset"]).reset_index(
                drop=True
            )

    def parse_input_file(self, room_filepath: str):
        # TODO: there must be a cleaner and more direct way to do this
        room_key = get(
            [
                room_key
                for room_key, filepath in self.zones.items()
                if filepath == room_filepath
            ]
        )

        with open(room_filepath, "r") as fp:
            datapoints = [datapoint.strip() for datapoint in fp.readlines()]

        for datapoint in datapoints:
            quantity_key = re.findall(r".*_(.*)-ms1", datapoint)[0]

            if quantity_key == "con":
                # "windows opening" is represented with "con" is the datapoints filenames,
                # but "con" is a reserved symbol in Windows operating system, and creating
                # a directory named "con" from WSL will make it unaccessible ("the handle
                # is invalid" error). That's why we name the quantity "windows" instead.
                quantity_key = "windows"

            step(f"Parsing measurements for datapoint {datapoint}")
            with open(f"{self.input_dir}/{datapoint}.csv", "r") as fp:
                for row in fp.readlines()[1:]:
                    datetime, value = row.strip().split(";")

                    if quantity_key == "windows":
                        if value in ("0.0", "1.0"):
                            # reverse the value so that we can count the opened windows
                            value = "1.0" if value == "0.0" else "0.0"
                        else:
                            print(
                                f"\n  Ignoring invalid entry {value} for {datetime} in {datapoint}"
                            )
                            continue

                    try:
                        yield (
                            self.to_datetime(datetime),
                            room_key,
                            quantity_key,
                            value,
                            # Feed "Dataset" column with the datapoint ID for windows
                            # quantity only: we will use it to sum the opened windows
                            # TODO: this is quite a dirty way to deal with the data
                            re.findall(r".+_.+_(.+)_.{3}-.{3}", datapoint)[0]
                            if quantity_key == "windows"
                            else None,
                        )
                    except self.BelowDateTimeLowerLimit:
                        pass
                    except self.AboveDateTimeUpperLimit:
                        break

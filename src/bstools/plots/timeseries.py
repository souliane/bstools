#!/usr/bin/python3
# -*- coding: utf-8 -*-
from collections import defaultdict
from functools import cached_property
from statistics import StatisticsError, mean
from typing import List

import numpy as np
import pandas as pd
from dateutil.parser import parse as parse_date
from dateutil.relativedelta import relativedelta
from pyarrow import feather  # @UnresolvedImport

from ..lib.logger import step
from ..lib.os import OSTools
from ..lib.tools import get
from ..plotlib import (
    AutoLineplot,
    Barplot,
    Boxplot,
    CumulativeDistribution,
    Dimension,
    Distribution,
    Histogram,
    ManyFilesProcessor,
    PsychrometricChart,
    Room,
    Violin,
)
from .absence_filter import AbsenceFilter

SUMMER_TIME_RANGES = (
    (parse_date("2019/03/31 02:00"), parse_date("2019/10/27 02:00")),
    (parse_date("2020/03/29 02:00"), parse_date("2020/10/25 02:00")),
)


def make_timeseries_for_quantities(quantities_setting):

    # determine the order because dict.keys() order is unpredictable
    quantities_setting_keys = list(quantities_setting)

    class QuantityValues(defaultdict):
        """Store a list of values for each quantity of `quantities_setting`."""

        def __init__(self):
            super().__init__(list)

        @property
        def values(self) -> List[float]:
            return [self.get_value(quantity) for quantity in quantities_setting_keys]

        def get_value(self, quantity: str):
            try:
                return mean(self[quantity])
            except StatisticsError:  # no value
                return np.nan  # @UndefinedVariable

    class Datasets(defaultdict):
        def __init__(self):
            super().__init__(QuantityValues)

    class Rooms(defaultdict):
        def __init__(self):
            super().__init__(Datasets)

    class TimeSeries(defaultdict):
        DATETIME_PERIOD = "15min"

        def __init__(self):
            super().__init__(Rooms)

        @classmethod
        def get_axis(cls, quantity_key: str) -> str:
            """Returns the axis name for given quantity."""
            return quantities_setting[quantity_key].axis

        @classmethod
        def get_axes(cls):
            """Returns axes names for all quantities."""
            return [cls.get_axis(quantity) for quantity in quantities_setting_keys]

        def add(self, datetime, room, quantity, value, dataset=None):
            self[datetime.round(self.DATETIME_PERIOD)][room][dataset][quantity].append(
                float(value)
            )

        def _to_array(self):
            def get_entry(datetime, room, dataset, quantity_values):
                return (
                    datetime,
                    datetime.date().isoformat(),
                    datetime.strftime("%H:%M"),
                    datetime.strftime("%A %H:%M"),
                    datetime.weekday(),
                    datetime.month_name(),
                    room,
                    dataset,
                    *(quantity_values.values),
                )

            return [
                get_entry(datetime, room, dataset, quantity_values)
                for datetime, rooms in self.items()
                for room, datasets in rooms.items()
                for dataset, quantity_values in datasets.items()
            ]

        def to_dataframe(self):
            df = pd.DataFrame.from_records(
                data=self._to_array(),
                columns=(
                    [
                        "DateTime",
                        "Date",
                        "Time",
                        "WeekdayTime",
                        "WeekdayIndex",
                        "Month",
                        "Room",
                        "Dataset",
                    ]
                    + self.get_axes()
                ),
                #  Note: feather doesn't support index="DateTime"
            )
            # TODO: avoid dirty filtering based on hard-coded axis name
            try:
                windows_axis = self.get_axis("windows")
            except KeyError:
                datasets = df["Dataset"].unique()
            else:
                datasets = df[df[windows_axis].isnull()]["Dataset"].unique()
            assert len(datasets) == 1 or None not in datasets
            return df

    return TimeSeries


class TimeSeriesProcessor(ManyFilesProcessor):

    FIX_SUMMER_TIME: bool = False
    PLOT_SEPARATE_DATASETS: bool = False
    PLOT_FOR_PERIOD_OF_ABSENCE: bool = False
    PLOT_PSYCHROMETRY: bool = False
    FILTER_OCCUPIED_HOURS: bool = True

    # Use this to skip plots that would be unreadable because of too much information,
    # or set to `None` to disable this "feature" and plot everything.
    TOO_MUCH_DATASETS_THRESHOLD: int = 4

    # Feel free to adapt those arbitrary values if necessary
    ACCEPTABLE_PRESENCE_SIZE_RATIO_RANGE = (0.375, 0.51)

    absence_filter: AbsenceFilter = NotImplemented
    null_dataset_name: str = NotImplemented

    class BelowDateTimeLowerLimit(Exception):
        pass

    class AboveDateTimeUpperLimit(Exception):
        pass

    class DateRangeNotInCache(Exception):
        pass

    def __init__(
        self,
        date_range: tuple,
        output_subdirs: List[str] = None,
        ignore_existing_dumps: bool = False,
        *args,
        **kwargs,
    ):
        output_subdirs = output_subdirs or []
        start, end = [parse_date(date) for date in date_range]
        assert start < end
        self.datetime_range = [start, end.replace(hour=23, minute=59, second=59)]
        output_subdirs.insert(0, self.months)

        super().__init__(*args, output_subdirs=output_subdirs, **kwargs)

        for key in self.settings.quantities:
            OSTools.create_directory(self.output_dir / key)

        self.rooms = self.settings.rooms
        self.room = Room.get_by_key(self.settings.rooms, self.settings.room_key)
        self.ignore_existing_dumps = ignore_existing_dumps

        self.TimeSeries = make_timeseries_for_quantities(self.settings.quantities)

    @classmethod
    def process_date_ranges(cls, init_kwargs: dict, date_ranges: list = None):
        date_ranges = date_ranges or cls.settings.DateRange.values()

        first_date_range = date_ranges[0]
        for date_range in date_ranges[0:]:
            if (date_range[0] and date_range[0] < first_date_range[0]) or (
                date_range[1] and date_range[1] > first_date_range[1]
            ):
                raise Exception(
                    "The first date range is used to cache the data and speed up"
                    " further processing. Because of this, it must contain all the "
                    f"following ones, but {date_range} is not in {first_date_range}."
                )

        for date_range in date_ranges:
            try:
                instance = cls(date_range=date_range, **init_kwargs)
            except FileExistsError:
                print(
                    f"Skipping date range={date_range} because output directory already exists."
                )
            else:
                try:
                    instance.process()
                except cls.DateRangeNotInCache:
                    print(
                        f"Skipping date range={date_range} because this is not in "
                        "cache. Maybe you should use --overwrite to clear the cache or "
                        "remove that date range from the list."
                    )

    @classmethod
    def fix_summer_time(cls, datetime):
        """Add one hour if `datetime` is in the summer time range"""
        # assume that the logger time is the normal one
        return (
            datetime + relativedelta(hours=1)
            if any(
                datetime > range_[0] and datetime < range_[1]
                for range_ in SUMMER_TIME_RANGES
            )
            else datetime
        )

    @cached_property
    def months(self):
        return self.datetime_range_to_month_names("%b")

    @cached_property
    def full_months(self):
        return self.datetime_range_to_month_names("%B")

    @cached_property
    def title_months(self):
        return self.months if len(self.full_months) > 12 else self.full_months

    @cached_property
    def timesteps_per_hour(self):
        # Note: "end" for assertion and return is not the same - this is made on purpose
        assert (
            pd.timedelta_range(
                start="00:00:00", end="01:00:00", freq=self.TimeSeries.DATETIME_PERIOD
            ).size
            > 1
        ), f"Timestep maximal value should be 1 hour: {self.TimeSeries.DATETIME_PERIOD}"
        return pd.timedelta_range(
            start="00:00:00", end="00:59:59", freq=self.TimeSeries.DATETIME_PERIOD
        ).size

    @property
    def zones(self):
        return self.settings.zones

    def datetime_range_to_month_names(self, format_code: str) -> str:
        months = [date.strftime(format_code) for date in self.datetime_range]
        return "-".join(months[:1] if len(set(months)) == 1 else months)

    def _to_datetime(self, datetime: str) -> pd.Timestamp:
        return pd.to_datetime(datetime)

    def to_datetime(self, datetime: str):
        datetime = self._to_datetime(datetime)
        if self.settings.timezone_offset:
            datetime = datetime + relativedelta(hours=self.settings.timezone_offset)

        if datetime < self.datetime_range[0]:
            raise self.BelowDateTimeLowerLimit
        elif datetime > self.datetime_range[1]:
            print("\rReached datetime upper limit: {}".format(self.datetime_range[1]))
            raise self.AboveDateTimeUpperLimit
        else:
            if self.FIX_SUMMER_TIME:
                datetime = self.fix_summer_time(datetime)
            return datetime

    def build_output_filepath(self, quantity, desc, df=None, room=None) -> str:
        assert (df is None) ^ (room is None)
        room = room or "_".join(sorted(df["Room"].unique()))
        desc = desc.replace(" ", "_").lower()
        return str(self.output_dir / quantity / f"{room}_{quantity}_{{}}_{desc}.png")

    def get_entity(
        self, room_name: str = None, desc: str = None
    ):  # used as description suffix for the graph title
        room_name = room_name or self.room.name
        return (
            f"{room_name} for {desc} in {self.title_months}"
            if desc
            else f"{room_name} in {self.title_months}"
        )

    def too_much_datasets(self, df):
        # use this to skip plots that would be unreadable because of too much information
        return self.TOO_MUCH_DATASETS_THRESHOLD is not None and (
            self.PLOT_SEPARATE_DATASETS
            and df["Dataset"].unique().size > self.TOO_MUCH_DATASETS_THRESHOLD
        )

    def _plot_by_room(self, plot_cls, df, quantity, desc, *args, **kwargs):
        for room in df["Room"].unique():
            room_name = Room.get_by_key(self.rooms, room).name
            plot_cls(entity=self.get_entity(room_name, desc)).plot(
                df=df[df.Room == room],
                *args,
                filepath=self.build_output_filepath(quantity, desc, room=room),
                **kwargs,
            )

    def plot_quantity(self, df, quantity, desc):
        axis = self.TimeSeries.get_axis(quantity)
        filepath = self.build_output_filepath(quantity, desc, df)

        entity = self.get_entity(desc=desc)
        dataset = "Dataset" if self.PLOT_SEPARATE_DATASETS else None

        for cls in (CumulativeDistribution, Distribution):
            if dataset:
                self._plot_by_room(cls, df, quantity, desc, x=axis, hue=dataset)
            else:
                cls(entity=entity).plot(df=df, x=axis, hue="Room", filepath=filepath)

        if not self.too_much_datasets(df):
            for cls in (Boxplot, Violin):
                cls(entity=entity).plot(
                    df=df,
                    y=axis,
                    x="Room" if dataset else "Month",
                    hue=dataset or "Room",
                    filepath=filepath,
                )

            if dataset:
                self._plot_by_room(Histogram, df, quantity, desc, x=axis, hue=dataset)
            else:
                Histogram(entity=entity).plot(
                    df=df, x=axis, hue="Room", filepath=filepath
                )

        for estimator in ("min", "max"):
            AutoLineplot(entity=entity, figure_size=Dimension(9, 5)).plot(
                df=df,
                x="Date",
                y=axis,
                estimator=estimator,
                hue=dataset or "Room",
                style="Room" if self.PLOT_SEPARATE_DATASETS else None,
                filepath=filepath,
            )

    def plot_quantity_profiles(self, df, quantity, desc):
        entity = self.get_entity(desc=desc)
        filepath = self.build_output_filepath(quantity, desc, df)

        kwargs = {
            "y": self.TimeSeries.get_axis(quantity),
            "hue": "Dataset" if self.PLOT_SEPARATE_DATASETS else "Room",
            "style": "Room" if self.PLOT_SEPARATE_DATASETS else None,
        }

        AutoLineplot(entity=entity, figure_size=Dimension(9, 5)).plot(
            df=df.sort_values(by=["Time", "Room", "Dataset"]),
            x="Time",
            estimator="mean",
            filepath=filepath.format(f"{{}}_vs_time"),
            **kwargs,
        )

        # The first entry may not be Monday (especially when holidays are filtered out)
        AutoLineplot(entity=entity, figure_size=Dimension(9, 5)).plot(
            df=df.sort_values(by=["WeekdayIndex", "Time", "Room", "Dataset"]),
            x="WeekdayTime",
            estimator="mean",
            filepath=filepath.format(f"{{}}_vs_weekday"),
            **kwargs,
        )

    def plot_continuous_quantity_profile(self, df, quantity, desc="continuous"):
        if not self.too_much_datasets(df):
            AutoLineplot(entity=self.get_entity(), figure_size=Dimension(9, 5)).plot(
                df,
                x="Date",
                y=self.TimeSeries.get_axis(quantity),
                estimator="max",
                hue="Dataset" if self.PLOT_SEPARATE_DATASETS else "Room",
                style="Room" if self.PLOT_SEPARATE_DATASETS else None,
                filepath=self.build_output_filepath(quantity, desc, df),
            )

    def plot_ummet_hours(self, df, quantity_key):
        axis = self.TimeSeries.get_axis(quantity_key)
        filepath = self.build_output_filepath(quantity_key, "unmet_hours", df)
        quantity = self.settings.quantities[quantity_key]
        min_, max_ = quantity.acceptable_range
        assert min_ or max_, f"At least one of {quantity.acceptable_range} must be set"

        datasets = df.Dataset.unique()
        if None in datasets:
            assert datasets.size == 1, "Can't have None dataset mixed with other values"

        def generate_rows(axis_series, criterium):
            for dataset in datasets:
                dataset_series = (
                    df.Dataset.isnull() if dataset is None else (df.Dataset == dataset)
                )
                yield [
                    dataset or self.null_dataset_name,
                    sum(dataset_series & axis_series) / self.timesteps_per_hour,
                    criterium,
                ]

        rows, title_desc = [], self.settings.quantities[quantity_key].name

        if min_ is not None:
            criterium = f"< {min_} {quantity.unit}"
            rows.extend(generate_rows(df[axis] < min_, criterium))
            if max_ is None:  # there will be no hue, so display the criterium in title
                title_desc += f" {criterium}"
        if max_ is not None:
            criterium = f"> {max_} {quantity.unit}"
            rows.extend(generate_rows(df[axis] > max_, criterium))
            if min_ is None:  # there will be no hue, so display the criterium in title
                title_desc += f" {criterium}"

        Barplot(entity=self.get_entity(desc=title_desc)).plot(
            df=pd.DataFrame(rows, columns=["Dataset", "Unmet Hours", "Criterium"]),
            x="Dataset",
            y="Unmet Hours",
            hue="Criterium",
            ci=None,
            filepath=filepath,
        )

    def get_dataset_from_filepath(self, filepath: str) -> str:  # @UnusedVariable
        return None

    def parse_input_file(self, *args, **kwargs):
        raise NotImplementedError

    def parse_input_files(self, filepaths: list, *args, **kwargs):
        sub_dfs = []

        for filepath in filepaths:
            dump_path = filepath.parent / (filepath.name + ".feather")

            if not self.ignore_existing_dumps and dump_path.exists():
                step(f"Reading dataframe dump from {dump_path}")
                cached_df = feather.read_feather(dump_path)
                min_, max_ = cached_df.DateTime.min(), cached_df.DateTime.max()
                if min_ > self.datetime_range[0] or max_ < self.datetime_range[1]:
                    print(f"  Dates of cached data are between {min_} and {max_}")
                cached_df = cached_df[
                    cached_df.DateTime.between(*self.datetime_range)
                    & (self.zones is None or cached_df.Room.isin(self.zones.keys()))
                ]
                dataset = self.get_dataset_from_filepath(filepath)
                if dataset:  # update dataset value after the directory has been renamed
                    current_dataset = get(cached_df.Dataset.unique())
                    if current_dataset != dataset:
                        print(
                            f"  Dataset in the dump file is {current_dataset} - changing to {dataset}"
                        )
                        cached_df.Dataset = dataset
                sub_dfs.append(cached_df)
            else:
                index = 0
                time_series = self.TimeSeries()
                for row in self.parse_input_file(filepath, *args, **kwargs):
                    time_series.add(*row)
                    index += 1
                    if not index % 1000:
                        print(f"\rParsed {index} entries... ", end="")
                print(f"\rParsed {index} entries... ")
                sub_df = self._pre_process_data(time_series.to_dataframe())
                sub_dfs.append(sub_df)
                step(f"Dumping dataframe to {dump_path}")
                sub_df.to_feather(dump_path)

        df = pd.concat(sub_dfs, sort=False, copy=False)
        if df.empty:
            raise self.DateRangeNotInCache

        # sort by "DateTime" here because Plot.plot() tries to save some computational
        # time and defaults to sort=False when calling seaborn functions
        df.sort_values(by=["DateTime", "Room", "Dataset"], inplace=True)

        return df

    def _pre_process_data(self, df: pd.DataFrame):
        return df

    def some_values_exist(self, df: pd.DataFrame, quantity: str):
        return sum(df[self.TimeSeries.get_axis(quantity)].notnull()) > 0

    def all_values_exist(self, df: pd.DataFrame, quantity: str):
        return df.size and sum(df[self.TimeSeries.get_axis(quantity)].isnull()) == 0

    def filter_occupied_hours(self, df: pd.DataFrame):
        if self.FILTER_OCCUPIED_HOURS:
            df_hours = df[
                df.DateTime.dt.time.isin(
                    pd.date_range(
                        *self.settings.occupied_hours,
                        freq=self.TimeSeries.DATETIME_PERIOD,
                    ).time
                )
            ]
            size_ratio = len(df_hours) / len(df)
            min_, max_ = self.ACCEPTABLE_PRESENCE_SIZE_RATIO_RANGE
            assert (
                size_ratio > min_ and size_ratio < max_
            ), f"Atypical size ratio for 'Occupied Hours'/'Occupied Days' = {size_ratio}"
            return df_hours
        else:
            return NotImplemented

    def process_data(self, df: pd.DataFrame, *args, **kwargs):  # @UnusedVariable
        step("Filtering presence and absence data...")
        df_presence, df_absence = self.absence_filter(df).split_presence_and_absence()
        df_presence_hours = self.filter_occupied_hours(df_presence)

        if self.FILTER_OCCUPIED_HOURS and self.PLOT_PSYCHROMETRY:
            for room in df["Room"].unique():
                for dataset in df["Dataset"].unique():
                    PsychrometricChart(
                        entity=self.get_entity(),
                        timesteps_per_hour=self.timesteps_per_hour,
                    ).plot(
                        df_presence_hours[
                            (df_presence_hours.Room == room)
                            & (
                                df_presence_hours.Dataset.isnull()
                                if dataset is None
                                else df_presence_hours.Dataset == dataset
                            )
                        ],
                        filepath=str(
                            self.output_dir
                            / "{}_psychro_unmet_hours.png".format(
                                room if dataset is None else f"{room}_{dataset}"
                            )
                        ),
                    )

        for key, quantity in self.settings.quantities.items():
            if self.some_values_exist(df, key):
                self.plot_continuous_quantity_profile(df, key)
                if self.some_values_exist(df_presence, key):
                    self.plot_quantity_profiles(df_presence, key, "Occupied Days")
                    if self.FILTER_OCCUPIED_HOURS and self.some_values_exist(
                        df_presence_hours, key
                    ):
                        self.plot_quantity(df_presence_hours, key, "Occupied Hours")
                        if quantity.acceptable_range:
                            self.plot_ummet_hours(df_presence_hours, key)
                    else:
                        self.plot_quantity(df_presence, key, "Occupied Days")
                if self.PLOT_FOR_PERIOD_OF_ABSENCE and self.some_values_exist(
                    df_absence, key
                ):
                    self.plot_quantity_profiles(df_absence, key, "Unoccupied Days")
                    self.plot_quantity(df_absence, key, "Unoccupied Days")

# -*- coding: utf-8 -*-
import os
import re
from collections import OrderedDict
from functools import cached_property
from typing import List

from dateutil.parser import ParserError
from dateutil.relativedelta import relativedelta

from ..lib.logger import step
from ..settings import Settings
from .absence_filter import CO2Monotony
from .timeseries import TimeSeriesProcessor


class EnergyplusBasedResultsProcessorMixin:
    result_filename = NotImplemented

    def __init__(self, *args, dirname: str, variants: list = None, **kwargs):
        super().__init__(*args, output_subdirs=[dirname], **kwargs)
        self.variants = variants

    def get_input_filepaths(self, *args, **kwargs) -> list:  # @UnusedVariable
        return (
            [
                self.input_dir / variant / self.result_filename
                for variant in self.variants
            ]
            if self.variants
            else [
                self.input_dir / dirname / self.result_filename
                for dirname in os.listdir(self.input_dir)
                if not dirname.startswith("_")  # ignore directories starting with "_"
            ]
        )


class EnergyplusResultsProcessor(
    EnergyplusBasedResultsProcessorMixin, TimeSeriesProcessor
):
    PLOT_SEPARATE_DATASETS = True
    PLOT_PSYCHROMETRY: bool = True

    absence_filter = CO2Monotony

    result_filename = "eplusout.csv"
    csv_separator = ","
    datetime_column_index = 0
    settings = Settings.EnergyplusResultsProcessor

    @classmethod
    def process_groups(
        cls, init_kwargs: dict, group: Settings.EnergyplusResultsProcessor.Group = None
    ):
        year = cls.settings.simulation_year
        for group in [group] if group else cls.settings.simulation_groups:
            cls.process_date_ranges(
                init_kwargs={
                    "ignore_existing_dumps": False,
                    "dirname": group.dirname,
                    "variants": group.variants,
                    **init_kwargs,
                },
                date_ranges=[
                    (f"{year}-{start}", f"{year}-{end}")
                    for start, end in (
                        group.date_ranges or cls.settings.DateRange.values()
                    )
                ],
            )

    @cached_property
    def simulation_year(self) -> int:
        years = set(datetime.year for datetime in self.datetime_range)
        assert len(years) == 1, f"Datetime range uses more than 1 year: {years}"
        return years.pop()

    def get_column_index(self, headers, zone_pattern, quantity_pattern):
        for index, header in enumerate(headers):
            if re.search(
                r"{}.*:.*{}.*\(TimeStep\)".format(zone_pattern, quantity_pattern),
                header,
            ):
                print(
                    f"Column found for '{zone_pattern}' and '{quantity_pattern}': "
                    f"'{header}'"
                )
                return index

        print(f"Column not found for '{zone_pattern}' and '{quantity_pattern}'")
        return None

    def get_columns_indices(self, headers: List[str]) -> OrderedDict:
        return OrderedDict(
            (
                (
                    (zone_key, quantity_key),
                    self.get_column_index(headers, zone_pattern, quantity.name),
                )
                for zone_key, zone_pattern in self.zones.items()
                for quantity_key, quantity in self.settings.quantities.items()
            )
        )

    def _to_datetime(self, datetime: str):
        # super()._to_datetime(datetime) would raise pd.errors.OutOfBoundsDatetime
        assert re.match(
            # datetimes from energyplus results might look strange and miss the year
            r"[ ]*[0-9]{2}/[0-9]{2}[ ]+[0-9]{2}:[0-9]{2}:[0-9]{2}",
            datetime,
        ), f"Not implemented datetime format: {datetime}"
        datetime = "{}/{}".format(
            self.simulation_year, datetime.strip().replace("  ", " ")
        )

        try:
            return super()._to_datetime(datetime)
        except ParserError:
            assert re.match(
                r"[0-9]{4}/[0-9]{2}/[0-9]{2} 24:[0-9]{2}:[0-9]{2}", datetime
            ), f"Not implemented datetime format: {datetime}"
            # 24:00:00 is not valid, so set it to 00:00:00 and add one day
            return super()._to_datetime(
                datetime.replace(" 24:", " 00:")
            ) + relativedelta(days=1)

    def get_dataset_from_filepath(self, filepath: str) -> str:
        return str(filepath).split("/")[-2]

    def parse_input_file(self, simulation_filepath: str):
        step(f"Parsing energyplus results in {simulation_filepath}")
        dataset = self.get_dataset_from_filepath(simulation_filepath)
        with open(simulation_filepath, "r") as fp:
            columns = self.get_columns_indices(
                headers=fp.readline().split(self.csv_separator)
            )

            for row in fp.readlines():
                cells = row.split(self.csv_separator)
                for zone_key in self.zones.keys():
                    for key in self.settings.quantities:
                        column_index = columns[(zone_key, key)]
                        if column_index:
                            try:
                                yield (
                                    self.to_datetime(cells[self.datetime_column_index]),
                                    zone_key,
                                    key,
                                    cells[column_index],
                                    dataset,
                                )
                            except self.BelowDateTimeLowerLimit:
                                pass
                            except self.AboveDateTimeUpperLimit:
                                break

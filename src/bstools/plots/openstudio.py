# -*- coding: utf-8 -*-
import json
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from functools import cached_property
from typing import Dict

import pandas as pd

from ..lib.logger import step
from ..plotlib import Barplot, StackedBarplots
from ..plotlib.processor import ManyFilesProcessor
from ..settings import Settings
from .energyplus import EnergyplusBasedResultsProcessorMixin


class SIUnit(Enum):
    kWh_per_m2 = "[kWh/m²]"
    kWh = "[kWh]"


@dataclass
class Quantity:
    name: str
    json_key: str = None
    types: Dict[str, str] = None  # Dict[quantity type, json_key]
    input_unit: SIUnit = None
    target_unit: SIUnit = None

    # this is not a setting, leave it to False!
    __already_displayed_kBtu_to_kWh_warning = False

    def __post_init__(self):
        assert bool(self.json_key) ^ bool(self.types)
        if self.target_unit:
            assert self.input_unit
        else:
            self.target_unit = self.input_unit

    @cached_property
    def axis(self):
        return (
            f"{self.name} {self.target_unit.value}" if self.target_unit else self.name
        )

    def fix_value(self, json_data, json_key):
        value = json_data[json_key]

        if json_key.startswith("fuel_"):
            # There's a bug on OpenStudioResults measure version 20201028T071516Z, we need
            # to convert some values from kBtu to kWh. See
            # https://unmethours.com/question/50515/openstudioresults-measures-do-not-convert-all-values-to-si-units/
            if not self.__class__.__already_displayed_kBtu_to_kWh_warning:
                self.__class__.__already_displayed_kBtu_to_kWh_warning = True
                print(
                    "\nWARNING: converting assumed kBtu values to kWh without checking the"
                    "\nmeasure version - maybe this has been fixed upstream and your value"
                    "\nis already in kWh. Look here for more information:"
                    "\nhttps://unmethours.com/question/50515/openstudioresults-measures-do-not-convert-all-values-to-si-units/\n"
                )
            value *= 0.2931

        if self.input_unit != self.target_unit:
            if self.input_unit == SIUnit.kWh and self.target_unit == SIUnit.kWh_per_m2:
                value /= json_data["total_building_area"]

        return (
            round(float(value), 2)
            if self.target_unit == SIUnit.kWh_per_m2
            else int(value)
        )


class OpenStudioResultsProcessor(
    EnergyplusBasedResultsProcessorMixin, ManyFilesProcessor
):
    result_filename = "results.json"
    settings = Settings.OpenStudioResultsProcessor

    # this is a setting, feel free to change
    convert_kWh_to_kWh_per_m2: bool = True

    quantities = (
        Quantity("Total Site EUI", "total_site_eui", input_unit=SIUnit.kWh_per_m2),
        Quantity(
            "Unmet Hours During Occupied Heating",
            "unmet_hours_during_occupied_heating",
        ),
        Quantity(
            "Unmet Hours During Occupied Cooling",
            "unmet_hours_during_occupied_cooling",
        ),
        Quantity(
            "End Use",
            types={
                "Heating": "end_use_heating",
                "Interior Lighting": "end_use_interior_lighting",
                "Pumps": "end_use_pumps",
                "Fans": "end_use_fans",
            },
            input_unit=SIUnit.kWh,
            target_unit=SIUnit.kWh_per_m2,
        ),
        Quantity(
            "End Use Electricity",
            types={
                "Heating": "end_use_electricity_heating",
                "Interior Lighting": "end_use_electricity_interior_lighting",
                "Pumps": "end_use_electricity_pumps",
                "Fans": "end_use_electricity_fans",
            },
            input_unit=SIUnit.kWh,
            target_unit=SIUnit.kWh_per_m2,
        ),
        Quantity(
            "Energy Use",
            types={
                "Electricity": "fuel_electricity",
                "District Heating": "fuel_district_heating",
            },
            input_unit=SIUnit.kWh,
            target_unit=SIUnit.kWh_per_m2,
        ),
    )

    @classmethod
    def process_groups(
        cls, init_kwargs: dict, group: Settings.EnergyplusResultsProcessor.Group = None
    ):
        for group in (
            [group] if group else Settings.EnergyplusResultsProcessor.simulation_groups
        ):
            try:
                instance = cls(
                    dirname=group.dirname, variants=group.variants, **init_kwargs
                )
            except FileExistsError:
                print(
                    f"Skipping group={group.dirname} because output directory already exists."
                )
            else:
                instance.process()

    def parse_input_files(self, filepaths: list, *args, **kwargs):  # @UnusedVariable
        results = defaultdict(list)

        for filepath in filepaths:
            dirname = str(filepath).split("/")[-2]
            step(f"Parsing openstudio results from {filepath}")
            with open(filepath) as fp:
                json_data = json.load(fp)["OpenStudio Results"]
                units = json_data["units"]
                assert units == "SI", f"OpenStudio results are using the {units} units!"

                for quantity in self.quantities:
                    if quantity.json_key:
                        results[quantity.name].append(
                            [
                                dirname,
                                quantity.fix_value(json_data, quantity.json_key),
                            ]
                        )
                    else:
                        for type_, json_key in quantity.types.items():
                            results[quantity.name].append(
                                [
                                    dirname,
                                    quantity.fix_value(json_data, json_key),
                                    type_,
                                ]
                            )

        dfs = {}
        for quantity in self.quantities:
            dfs[quantity.name] = pd.DataFrame(
                results[quantity.name],
                columns=["Dataset", quantity.axis]
                if quantity.json_key
                else ["Dataset", quantity.axis, "Type"],
            )
        return dfs

    def process_data(self, data: dict, *args, **kwargs):  # @UnusedVariable
        for quantity in self.quantities:
            # uncomment to sort by axis, but it's usually better to keep the initial order
            # data[quantity.name].sort_values(by=axis, inplace=True)

            filepath = self.output_dir / "{}.png".format(quantity.name.replace(" ", ""))

            if quantity.json_key:
                Barplot("NMS Grein").plot(
                    df=data[quantity.name],
                    x="Dataset",
                    y=quantity.axis,
                    ci=None,
                    filepath=filepath,
                )
            else:
                StackedBarplots("NMS Grein").plot(
                    df=data[quantity.name],
                    x="Dataset",
                    y=quantity.axis,
                    hue="Type",
                    filepath=filepath,
                )

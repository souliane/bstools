# -*- coding: utf-8 -*-
import os
from collections import OrderedDict
from enum import Enum
from pathlib import Path

import pandas as pd

from ..plotlib.image import Dimension
from ..plotlib.plot import AutoLineplot
from ..plotlib.processor import ManyFilesProcessor
from ..settings import Quantity, Settings


class SIUnit(Enum):
    kWh_per_m2 = "[kWh/m²]"
    kWh = "[kWh]"


class WeatherFilesProcessor(ManyFilesProcessor):

    location_column = 1  # on the first row only

    start_row = 8
    year_column = 0
    month_column = 1
    day_column = 2
    hour_column = 3
    quantity_columns = OrderedDict(
        (
            (Quantity.temperature, 6),
            (Quantity.relative_humidity, 8),
        )
    )

    settings = Settings.WeatherFilesProcessor

    def get_input_filepaths(self, *args, **kwargs) -> list:
        return [
            Path(self.input_dir / filepath)
            for filepath in os.listdir(self.input_dir)
            if filepath.lower().endswith(".epw")
        ]

    def parse_input_files(self, filepaths: list, *args, **kwargs):
        results = []

        for filepath in filepaths:
            print(f"Processing {filepath}")
            with open(filepath) as fp:
                location = fp.readline().split(",")[self.location_column]
                index = 0
                for row in fp.readlines()[self.start_row - 1 :]:
                    row = row.split(",")
                    datetime = pd.to_datetime(
                        "{year}/{month}/{day} {hour:02}".format(
                            year=int(row[self.year_column]),
                            month=int(row[self.month_column]),
                            day=int(row[self.day_column]),
                            hour=int(row[self.hour_column]) - 1,
                        ),
                        format="%Y%m%d %H",
                    )

                    if index == 0:  # sanity check
                        assert datetime.month == 1
                        assert datetime.day == 1
                        assert datetime.hour == 0

                    results.append(
                        [
                            datetime,
                            datetime.date().isoformat(),
                            datetime.strftime("%H:%M"),
                            datetime.strftime("%A %H:%M"),
                            datetime.weekday(),
                            datetime.month_name(),
                            location,
                            *(
                                float(row[column])
                                for column in self.quantity_columns.values()
                            ),
                        ]
                    )
                    index += 1
                    if not index % 1000:
                        print(f"\rParsed {index} entries... ", end="")
                assert index in (
                    8760,
                    8784,
                ), f"Parsed {index} entries - EPW is not valid"
                print(f"\rParsed {index} entries... ")

        return pd.DataFrame.from_records(
            data=results,
            columns=(
                [
                    "DateTime",
                    "Date",
                    "Time",
                    "WeekdayTime",
                    "WeekdayIndex",
                    "Month",
                    "Location",
                ]
                + [quantity().axis for quantity in self.quantity_columns.keys()]
            ),
        )

    def process_data(self, data, *args, **kwargs):
        data.sort_values(by=["Time", "Location"])

        for month in ("January", "June"):
            for quantity in self.quantity_columns.keys():
                quantity = quantity()
                for estimator in ("min", "max", "mean"):
                    AutoLineplot(entity=month, figure_size=Dimension(9, 5)).plot(
                        df=data[data.Month == month],
                        x="Time",
                        y=quantity.axis,
                        estimator=estimator,
                        filepath=str(
                            self.output_dir
                            / f"{quantity.normalized_name}_{month}_vs_time.png"
                        ),
                        hue="Location",
                    )

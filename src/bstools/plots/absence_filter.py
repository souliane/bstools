#!/usr/bin/python3
# -*- coding: utf-8 -*-
from datetime import date
from functools import cached_property
from itertools import chain

import pandas as pd
from dateutil.relativedelta import relativedelta

from ..lib.logger import step


def dataframe_difference(df: pd.DataFrame, sub_df: pd.DataFrame) -> pd.DataFrame:
    return df[~df.index.isin(sub_df.index)]


class AbsenceFilter:
    message: str = "Absence according to {}:\n  {}"

    def __init__(self, df: pd.DataFrame):
        # energyplus "01/01/10 24:00" ends in pandas "01/01/11 00:00"
        self.df = df  # [df.Time != "00:00"]

    @classmethod
    def get_unique_days(self, df: pd.DataFrame) -> list:
        return list(df.DateTime.dt.date.unique())

    @classmethod
    def print_days(cls, days: list):
        ranges = []
        if days:
            starting_day = ending_day = days[0]
            # add None to process the last item as any other
            for day in days[1:] + [None]:
                if day != ending_day + relativedelta(days=1):
                    ranges.append(
                        f"{starting_day} - {ending_day}"
                        if starting_day != ending_day
                        else f"{starting_day}"
                    )
                    starting_day = ending_day = day
                else:
                    ending_day = day

        print(cls.message.format(cls.__name__, "\n  ".join(ranges)))

    @cached_property
    def absence_days(self) -> list:
        raise NotImplementedError

    def split_presence_and_absence(self):
        self.print_days(self.absence_days)
        df_presence = self.df[~self.df.DateTime.dt.date.isin(self.absence_days)]
        return df_presence, dataframe_difference(self.df, df_presence)


class Weekends(AbsenceFilter):
    @cached_property
    def absence_days(self):
        # 5 stands for Saturday
        return self.get_unique_days(self.df[self.df.DateTime.dt.weekday >= 5])


class Holidays(AbsenceFilter):
    # Holidays between 01/09/2019 and 06/09/2021
    HOLIDAYS = [
        "2019-09-01",  # school starts on 02/09/2019
        (
            "2019-10-26",
            "2019-11-02",
        ),  # National holiday + fall holidays + Allerseelentag
        "2019-11-15",  # Festtage der Landespatrone Niederösterreich
        "2019-12-08",  # Mariä Empfängnis
        ("2019-12-23", "2020-01-06"),  # christmas holidays
        ("2020-02-03", "2020-02-08"),  # semester holidays
        ("2020-04-04", "2020-04-14"),  # easter holidays
        "2020-05-01",  # Public holiday
        ("2020-05-21", "2020-05-22"),  # Christi Himmelfahrt + Schulautonome Tage NÖ
        ("2020-05-30", "2020-06-02"),  # Pfingstferien
        ("2020-06-11", "2020-06-12"),  # Fronleichnam + Schulautonome Tage NÖ
        (
            "2020-07-04",
            "2020-09-06",
        ),  # school ends on 03/07/2019 and restarts on 07/09/2020
        (
            "2020-10-26",
            "2020-11-02",
        ),  # National holiday + fall holidays + Allerseelentag
        "2020-11-15",  # Festtage der Landespatrone NÖ
        "2020-12-08",  # Mariä Empfängnis
        ("2020-12-24", "2021-01-06"),  # christmas holidays
        ("2021-02-01", "2021-02-06"),  # semester holidays
        ("2021-03-27", "2021-04-05"),  # easter holidays
        "2021-05-01",  # Public holiday
        "2021-05-13",  # Christi Himmelfahrt
        ("2021-05-22", "2021-05-24"),  # Pfingstferien
        ("2021-06-03"),  # Fronleichnam
        (
            "2021-07-03",
            "2021-09-05",
        ),  # school ends on 02/07/2021 and restarts on 06/09/2021
    ]

    @cached_property
    def absence_days(self):
        return [
            day
            for day in chain(
                *(
                    [date.fromisoformat(holiday)]
                    if isinstance(holiday, str)
                    else pd.date_range(*holiday).date
                    for holiday in self.HOLIDAYS
                )
            )
            if day in self.get_unique_days(self.df)
        ]


class WeekendsAndHolidays(AbsenceFilter):
    @cached_property
    def absence_days(self):
        return sorted(
            set(Weekends(self.df).absence_days).union(Holidays(self.df).absence_days)
        )


class CO2BasedAlgorithm(AbsenceFilter):
    axis: str = "CO2 [ppm]"
    room_key: str = None

    @cached_property
    def absence_days(self):
        studied_room = self.room_key or self.df.Room.values[0]
        dataset = self.df.Dataset.values[0]
        step(
            f"{self.__class__.__name__} will process data for room '{studied_room}' and dataset '{dataset}'..."
        )
        return [
            date.fromisoformat(day)
            for day in self.df.Date.unique()
            if self.detect_absence(
                self.df[
                    (self.df.Date == day)
                    & (self.df.Room == studied_room)
                    & (self.df.Dataset == dataset)
                ][self.axis]
            )
        ]

    @classmethod
    def detect_absence(cls, co2_values):
        raise NotImplementedError


class CO2Monotony(CO2BasedAlgorithm):
    @classmethod
    def detect_absence(cls, co2_values):
        # round to the closest multiple of 10
        return co2_values.round(-1).is_monotonic_decreasing


class CO2StandardDeviation(CO2BasedAlgorithm):
    @classmethod
    def detect_absence(cls, co2_values):
        return co2_values.std() < 50


class CO2Range(CO2BasedAlgorithm):
    @classmethod
    def detect_absence(cls, co2_values):
        return co2_values.max() - co2_values.min() < 300


class WeekendsAndHolidaysAndCO2Monotony(AbsenceFilter):
    @cached_property
    def absence_days(self):
        days_fixed = WeekendsAndHolidays(self.df).absence_days
        days_detected = CO2Monotony(self.df).absence_days
        only_fixed = sorted(set(days_fixed).difference(days_detected))
        only_detected = sorted(set(days_detected).difference(days_fixed))

        if only_fixed:
            raise Exception(
                f"Absence detected in WeekendsAndHolidays but not in CO2Monotony: {only_fixed}"
            )
        if only_detected:
            raise Exception(
                f"Absence detected in CO2Monotony but not in WeekendsAndHolidays: {only_detected}"
            )

        return sorted(set(days_fixed).union(days_detected))

# -*- coding: utf-8 -*-

from dataclasses import dataclass, field
from pathlib import Path
from typing import List

from .image import Background, Dimension, Matrix
from .plot import Extrapolation


@dataclass
class Lighting:
    rotation: Matrix.Rotation = None  # @UndefinedVariable
    extrapolation: Extrapolation = None


@dataclass
class Room:
    key: str
    name: str = None
    volume: float = None

    figure_size: Dimension = None
    background_offsets: Dimension = None
    lighting: Lighting = field(default_factory=Lighting)

    background: Background = field(init=False)
    input_dir: Path = field(init=False)

    def __post_init__(self):
        if self.background_offsets:
            self.background = Background(
                self.input_dir / f"{self.key}.png",
                self.background_offsets,
            )

        if not self.name:
            self.name = self.key

        if self.figure_size:
            self.figure_size.x /= 100
            self.figure_size.y /= 100

    @classmethod
    def get_by_key(cls, rooms: List["Room"], key: str):
        rooms = [room for room in rooms if room.key == key]
        assert len(rooms) == 1, f"Found for room key '{key}': {rooms}"
        return rooms[0]

    @property
    def normalized_name(self):
        return self.name.replace(" ", "").replace("'", "").lower()

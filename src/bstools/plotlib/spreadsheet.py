# -*- coding: utf-8 -*-

from pathlib import Path

import numpy as np
from pyexcel_ods import get_data as ods_get_data

from .processor import FileProcessor


class ODSProcessor(FileProcessor):
    "LibreOffice OpenData Sheet processor"

    @classmethod
    def get_max_column_index(cls, values):
        """Return the highest column index for which a value is defined."""
        return max(len(row) for row in values)

    @classmethod
    def get_first_empty_column_index(cls, values):
        """Return the first column for which all values are empty."""
        index = -1
        for index in range(cls.get_max_column_index(values)):
            for row in values:
                try:
                    if row[index]:
                        break
                except IndexError:
                    return index
            if not row[index]:
                return index
        return index + 1

    @classmethod
    def filter_values(cls, values, start=0, end=None):
        """Filter values with the given columns indices (start and end)."""
        if end is None:
            end = cls.get_max_column_index(values)
        return [
            [cell or np.nan for cell in row[start:end]]  # @UndefinedVariable
            for row in values
            if row
        ]

    @classmethod
    def is_defined(cls, array, j, i):
        """Tells if the cell of column i and row j is defined."""
        try:
            return array[j][i] is not None
        except IndexError:
            return False

    @classmethod
    def percent_values(cls, a, b):
        """Return percent values of a/b."""
        assert len(a) == len(b)
        return [
            [
                (
                    round(100 * a[j][i] / b[j][i], 2)
                    if cls.is_defined(a, j, i) and cls.is_defined(b, j, i)
                    else None
                )
                for i in range(len(a[j]))
            ]
            for j in range(len(a))
        ]

    def process_sheet(self, sheet_name, values):
        raise NotImplementedError

    def parse_input_file(self, filepath: Path):
        return ods_get_data(str(filepath))

    def process_data(self, data, skip_first_row=True):
        prefix = self.settings.sheet_name_prefix
        for sheet_name, values in data.items():

            if skip_first_row:
                values = values[1:]

            if not values:
                continue

            if prefix is None or sheet_name.startswith(prefix):
                self.process_sheet(sheet_name, values)

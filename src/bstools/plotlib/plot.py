# -*- coding: utf-8 -*-

import os
from collections import OrderedDict
from dataclasses import dataclass
from enum import Enum
from functools import cached_property
from typing import List

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib import rcParams
from matplotlib.ticker import FuncFormatter, MultipleLocator
from pandas.core.frame import DataFrame
from pandas.core.series import Series
from psychrochart import PsychroChart, load_config

from ..lib.tools import get
from .image import Background, Dimension, PNGImage

DEBUG = os.getenv("DEBUG", False)


class Extrapolation(Enum):
    # Don't change these values! They are set according to pandas bfill and ffill methods
    rows = 0
    columns = 1


@dataclass
class ValueRange:
    min: float
    max: float
    levels: List[float] = None


class Plot:
    color_palette: str = None
    tight_layout: bool = False
    figure_size: Dimension = None

    accept_vmin_vmax_attributes: bool = False
    accept_levels_attribute: bool = False
    accept_sort_attribute: bool = False
    accept_data_attribute: bool = True

    add_hue_statistics_threshold: int = 0
    legend_outside: bool = False
    legend_inside: bool = False

    func: callable = NotImplemented
    title: str = NotImplemented

    def __init__(
        self,
        entity: str,
        figure_size: Dimension = None,
        background: Background = None,
        extrapolation: Extrapolation = None,
    ):
        if self.title is NotImplemented:
            self.title = self.__class__.__name__

        self.entity = entity
        self.background = background

        sns.set_theme()
        self.figure = self.reset_figure(figure_size=figure_size or self.figure_size)

        if self.color_palette:
            sns.color_palette(self.color_palette)

        self.extrapolation = extrapolation

        assert not all((self.legend_outside, self.legend_inside))

    @cached_property
    def normalized_title(self) -> str:
        return self.title.replace(" ", "_").lower()

    @staticmethod
    def reset_figure(figure_size: Dimension = None):
        """Start a new figure"""
        plt.close(fig="all")
        plt.clf()
        return plt.figure(figsize=figure_size.to_tuple() if figure_size else None)

    def set_title(
        self,
        title: str = None,
        x: str = None,
        y: str = None,
        estimator: str = None,
        ci: str = None,
    ):
        """Set figure title"""
        if title:  # forced title with no suptitle
            suptitle, title = None, title.title()
        else:
            suptitle = self.title.title()  # display type of the chart as suptitle
            if estimator:  # add "Min", "Max" or "Mean" before the quantity name
                y = "{} {}".format(estimator.title(), y)
            if ci:  # add confidence interval
                y = f"{y} with ci={ci}"
            axes = f"{y} vs {x}" if (x and y) else (x or y)
            title = f"{axes} in {self.entity}"

        if suptitle:
            if rcParams["figure.titlesize"] == "large":
                plt.suptitle(suptitle, fontsize="medium")
            else:
                plt.suptitle(suptitle)

        if len(title) > 60:
            plt.title(title, fontsize=10)
        else:
            plt.title(title)

    @staticmethod
    def export_to_png(filepath: str):
        """Export the current figure to PNG"""
        plt.savefig(filepath)
        print(f"Saved to {filepath}")

    def overlay_background(self, filepath: str):
        """Add background image to the current figure"""
        PNGImage.overlay_background(filepath, self.background)

    @classmethod
    def get_statistical_info(cls, df: DataFrame, axis: str, y: str):
        """Get standard deviation, min and max values."""
        return {
            entity: f"{entity}\n"
            + "\n".join(
                "{} = {:.2f}".format(func, getattr(series, func)())
                for func in ("std", "min", "max")
            )
            for entity, series in (
                (entity, df[df[axis] == entity][y]) for entity in df[axis].unique()
            )
        }

    @staticmethod
    def get_cbar_kws(label: str, fmt: str = None):
        if fmt is None:
            fmt = ".0f"

        def formatter(x, pos):  # @UnusedVariable
            return "{{:{}}}".format(fmt).format(x)

        return {"label": label, "format": FuncFormatter(formatter)}

    def plot(
        self,
        filepath: str,
        filename_x: str = None,
        title: str = None,
        title_x=None,
        title_y=None,
        value_range: ValueRange = None,
        decorate_graph_kwargs: dict = None,
        **kwargs,
    ):
        assert "df" in kwargs, "We need at least a 'df' attributes for plotting"

        if value_range:
            assert not any(arg in kwargs for arg in ("vmin", "vmax"))
            if self.accept_vmin_vmax_attributes:
                kwargs["vmin"] = value_range.min
                kwargs["vmax"] = value_range.max
            else:
                plt.ylim(value_range.min, value_range.max)
            if value_range.levels and self.accept_levels_attribute:
                kwargs["levels"] = value_range.levels

        df, hue, style = (
            kwargs["df"],
            kwargs.get("hue", None),
            kwargs.get("style", None),
        )
        if style:
            if df[style].unique().size < 2:
                kwargs.pop("style")
        if hue:
            hue_size = df[hue].unique().size
            if hue_size < 2:
                kwargs.pop("hue")
            elif hue_size <= self.add_hue_statistics_threshold:
                assert not kwargs.get(
                    "style", None
                ), "Too much information would make the plot difficult to read"
                kwargs["hue"] = df[hue].map(
                    self.get_statistical_info(df, hue, kwargs["y"])
                )
                self.legend_outside = True

        # use __class__ to avoid passing self as first func argument
        seaborn_func = self.__class__.func

        if self.extrapolation:
            axis = self.extrapolation.value
            kwargs["df"] = kwargs["df"].bfill(axis=axis).ffill(axis=axis)

        if self.accept_sort_attribute:
            kwargs.setdefault("sort", False)  # for performance purpose

        if self.accept_data_attribute:
            ax = seaborn_func(data=kwargs.pop("df"), **kwargs)
        else:
            # Some matplotlib methods won't work if you pass the dataframe
            # in kwargs, you must pass it as a positional parameter...
            ax = seaborn_func(kwargs.pop("df"), **kwargs)

        if self.legend_outside:
            ax.legend(
                loc="upper left",
                bbox_to_anchor=(1.01, 1),
                borderaxespad=0,
                fontsize="xx-small",
                title=hue,
            )
            self.tight_layout = True
        elif self.legend_inside:
            ax.legend(
                loc="best",
                fontsize="xx-small",
                title=hue,
            )

        self.decorate_graph(ax, **decorate_graph_kwargs or {})
        self.set_title(
            title=title,
            x=title_x,
            y=title_y,
            estimator=kwargs.get("estimator", None),
            ci=kwargs.get("ci", None),
        )

        if self.tight_layout:
            plt.tight_layout()

        filepath = str(filepath)
        if "estimator" in kwargs:  # estimator can be "mean", "max", "min"...
            assert filepath.endswith(".png")
            filepath = "{}_{}.png".format(filepath[:-4], kwargs["estimator"])

        filepath = str(filepath).format(
            "{}_vs_{}".format(self.normalized_title, filename_x.lower())
            if filename_x
            else self.normalized_title
        )
        self.export_to_png(filepath)
        if self.background:
            self.overlay_background(filepath)

    def fix_x_axis(self, xaxis, **kwargs):
        count = len(xaxis.get_majorticklabels())
        if count > 1:
            if count > 20:
                kwargs["fontsize"] = "x-small"
            plt.xticks(rotation=35, horizontalalignment="right", **kwargs)

    def decorate_graph(self, ax, *args, **kwargs):
        pass


class Heatmap(Plot):
    func = sns.heatmap
    color_palette = "Reds"
    accept_vmin_vmax_attributes = True

    def plot(self, df: DataFrame, label: str, fmt: str = ".0f", **kwargs):
        """Export heatmap to PNG file."""
        super().plot(
            df=df,
            annot=True,
            fmt=fmt,
            linewidths=0.5,
            xticklabels=False,
            yticklabels=False,
            cbar_kws=self.get_cbar_kws(label, fmt),
            mask=df.isnull(),
            cmap=self.color_palette,
            **kwargs,
        )


class Isolines(Plot):
    func = plt.contourf
    color_palette = "Reds"
    accept_vmin_vmax_attributes = True
    accept_levels_attribute = True
    accept_data_attribute: bool = False

    def plot(self, df: DataFrame, **kwargs):
        """Export isolines to PNG file."""
        super().plot(df=df.iloc[::-1], cmap=self.color_palette, **kwargs)

    def decorate_graph(self, cs, label: str, fmt: str, annotate_levels=False):
        plt.axis("off")  # remove axis

        if annotate_levels:
            plt.clabel(cs, fmt="%{}".format(fmt), colors="k", fontsize=14)

        cbar_kws = self.get_cbar_kws(label, fmt)
        cb = self.figure.colorbar(cs, **cbar_kws)  # add color bar
        cb.outline.set_linewidth(0)


class Lineplots(Plot):
    """Display several graphs next to each other on the same figure"""

    func = sns.relplot
    accept_sort_attribute: bool = True

    def plot(self, **kwargs):
        super().plot(
            kind="line",
            zorder=5,
            col_wrap=3,  # the number of graphs per row
            height=10,
            aspect=2,
            **kwargs,
        )

    def decorate_graph(self, g, major_ticks_step: int = None):
        for ax in g.axes.flat:
            ax.set_xticklabels(
                ax.get_xticklabels(), rotation=35, horizontalalignment="right"
            )
            if major_ticks_step:
                ax.xaxis.set_major_locator(MultipleLocator(major_ticks_step))


class Lineplot(Plot):
    func = sns.lineplot
    tight_layout = True
    accept_sort_attribute: bool = True

    def plot(self, df: DataFrame, x, y, **kwargs):
        super().plot(df=df, x=x, y=y, title_x=x, title_y=y, **kwargs)

    def decorate_graph(
        self,
        ax,
        major_ticks_step: int = None,
        minor_ticks_step: int = None,
        fill_between: tuple = None,
    ):
        if major_ticks_step:
            ax.xaxis.set_major_locator(MultipleLocator(major_ticks_step))
        if minor_ticks_step:
            ax.xaxis.set_minor_locator(MultipleLocator(minor_ticks_step))

        self.fix_x_axis(ax.xaxis)

        if fill_between:
            ax.fill_between(*fill_between, alpha=0.2, color="tab:green")


class AutoLineplot(Lineplot):
    """Lineplot with some attempts of decoration auto-configuration"""

    title = "Lineplot"
    add_hue_statistics_threshold: int = 4

    def _get_value(self, df: DataFrame, axis: str, values: dict):
        length = df[axis].unique().size
        if DEBUG:
            print(f"Axis length for {axis} is {length}")
        for threshold, value in values.items():
            if length <= threshold:
                return value
        raise NotImplementedError(f"Missing threshold range for length {length}")

    def plot(self, df: DataFrame, x, y, hue, **kwargs):
        decorate_kwargs = kwargs.setdefault("decorate_graph_kwargs", {})
        decorate_kwargs.setdefault(
            "major_ticks_step",
            self._get_value(df, x, {31: 2, 96: 4, 366: 14, 480: 24}),
        )
        decorate_kwargs.setdefault(
            "minor_ticks_step", self._get_value(df, x, {31: 2, 96: 1, 366: 1, 480: 4})
        )
        kwargs.setdefault(
            "ci",
            self._get_value(df, hue, {2: 95, 99: None})
            if kwargs.get("estimator", None) == "mean"
            else None,
        )

        super().plot(df=df, x=x, y=y, hue=hue, **kwargs)


class Boxplot(Plot):
    func = sns.boxplot
    add_hue_statistics_threshold: int = 4

    def plot(self, df: DataFrame, y: str, x: str, **kwargs):
        super().plot(
            df=df,
            y=y,
            x=x,
            title_y=y,
            **kwargs,
        )

    def decorate_graph(self, ax, *args, **kwargs):  # @UnusedVariable
        # remove X axis labels if there's only one value to display
        if not any(
            (
                len(ax.xaxis.get_minorticklabels()) > 1,
                len(ax.xaxis.get_majorticklabels()) > 1,
            )
        ):
            ax.set_xticklabels([])
            ax.set(xlabel=None)


class Violin(Boxplot):
    func = sns.violinplot
    accept_sort_attribute: bool = True

    def plot(self, df: DataFrame, hue: str = None, **kwargs):
        super().plot(df=df, hue=hue, split=df[hue].unique().size == 2, **kwargs)


class CumulativeDistribution(Plot):
    func = sns.ecdfplot
    title = "Cumulative Distribution Chart"

    def plot(self, x: str, **kwargs):
        super().plot(x=x, title_x=x, **kwargs)


class Distribution(Plot):
    func = sns.kdeplot
    title = "Distribution Chart"
    tight_layout = True

    def plot(self, x: str, **kwargs):
        super().plot(x=x, title_x=x, fill=True, **kwargs)


class Histogram(Plot):
    func = sns.histplot

    def plot(self, x: str, **kwargs):
        super().plot(x=x, title_x=x, **kwargs)


class Trendchart(Plot):
    func = sns.scatterplot
    tight_layout = True
    figure_size = Dimension(20, 10)

    def plot(self, df: DataFrame, x: str, y: str, style=None, **kwargs):
        super().plot(
            df=df,
            x=x,
            y=y,
            edgecolor="none",
            markers=True if style else r"$\cdot$",
            style=style,
            filename_x=x,
            title_x=x,
            title_y=y,
            **kwargs,
        )

    def decorate_graph(self, ax, major_ticks_step: int = None):
        if major_ticks_step:
            ax.xaxis.set_major_locator(MultipleLocator(major_ticks_step))
            self.fix_x_axis(ax.xaxis)


class Barplot(Plot):
    func = sns.barplot
    tight_layout: bool = True
    annotate_bars: bool = True

    def plot(self, df: DataFrame, x: str, y: str, **kwargs):
        super().plot(
            df=df,
            x=x,
            y=y,
            filename_x=x,
            title_x=x,
            title_y=y,
            **kwargs,
        )

    def decorate_graph(self, ax):
        self.fix_x_axis(ax.xaxis)

        if self.annotate_bars:
            for patch in ax.patches:
                ax.annotate(
                    format(patch.get_height(), ".0f"),
                    (patch.get_x() + patch.get_width() / 2, patch.get_height()),
                    ha="center",
                    va="center",
                    size=10,
                    xytext=(0, -10 if patch.get_height() > 15 else 9),
                    textcoords="offset points",
                )


class Barplots(Barplot):
    func = sns.catplot

    def plot(self, **kwargs):
        super().plot(kind="bar", **kwargs)

    def decorate_graph(self, ax, **kwargs):
        self.fix_x_axis(ax.ax.xaxis)


class StackedBarplots(Barplot):
    title = "Stacked Barplots"
    accept_data_attribute: bool = False
    annotate_bars: bool = False
    legend_inside: bool = True

    @classmethod
    def func(cls, df, x, y, hue, **kwargs):
        assert hue is not None

        for x_value in df[x].unique():
            cumulation = 0
            for hue_value in df[hue].unique():
                cumulation += get(
                    df[(df[x] == x_value) & (df[hue] == hue_value)][y].values
                )
                df.loc[(df[x] == x_value) & (df[hue] == hue_value), y] = cumulation

        for index, hue_value in enumerate(df[hue].unique()[::-1]):
            ax = sns.barplot(
                x=x,
                y=y,
                data=df[df[hue] == hue_value],
                color=sns.color_palette()[index],
                edgecolor="w",
                label=hue_value,
                **kwargs,
            )
        return ax


class PsychrometricChart:
    only_unmet_hours: bool = True
    co2_thresholds: list = OrderedDict(
        (
            (800, "blue"),
            (1000, "green"),
            (1500, "orange"),
            (2000, "red"),
            (None, "black"),
        )
    )

    def __init__(self, entity: str, timesteps_per_hour: int):
        self.entity = entity
        self.timesteps_per_hour = timesteps_per_hour

    @property
    def temperature(self):
        from ..settings import Quantity

        return Quantity.temperature()

    @property
    def relative_humidity(self):
        from ..settings import Quantity

        return Quantity.relative_humidity()

    @property
    def carbon_dioxide(self):
        from ..settings import Quantity

        return Quantity.carbon_dioxide()

    def create_chart(self):
        hours = "Unmet Hours" if self.only_unmet_hours else "Occupied Hours"

        config = load_config("default")
        config["figure"]["title"] = f"Psychrometric Chart for {hours} of {self.entity}"
        config["figure"]["partial_axis"] = True
        config["figure"]["x_axis"]["linewidth"] = 1
        config["figure"]["y_axis"]["linewidth"] = 1
        config["limits"].update(
            {
                "altitude_m": 550,
                "range_temp_c": [10, 40],
                "range_humidity_g_kg": [0, 25],
            }
        )
        config["saturation"]["linewidth"] = 1
        config["chart_params"].update(
            {
                "constant_rh_curves": self.relative_humidity.acceptable_range,
                "constant_rh_labels": self.relative_humidity.acceptable_range,
                "constant_rh_labels_loc": 0.1,
                "with_constant_v": False,
                "with_constant_h": False,
                "with_constant_wet_temp": False,
                "with_constant_dry_temp": True,
                "with_constant_humidity": True,
                "with_constant_rh": True,
                "constant_temp_label": None,
                "constant_humid_label": None,
                "constant_rh_label": None,
                "constant_temp_step": 2,
                "constant_temp_label_step": 2,
                "constant_temp_label_include_limits": False,
                "constant_humid_step": 10,
                "constant_humid_label_step": 10,
                "constant_humid_label_include_limits": False,
                "with_zones": False,
            }
        )
        chart = PsychroChart(config)
        chart.append_zones(
            {
                "zones": [
                    {
                        "zone_type": "dbt-rh",
                        "style": {
                            "edgecolor": [0.5, 0.5, 0.5, 1.0],
                            "facecolor": [0.5, 0.5, 0.5, 0.0],
                            "linewidth": 2,
                            "linestyle": "-",
                        },
                        "points_x": self.temperature.acceptable_range,
                        "points_y": self.relative_humidity.acceptable_range,
                        "label": "Acceptable\nPsychrometric\nRange",
                    },
                ]
            }
        )
        return chart

    def filter_unmet_hours(self, df: DataFrame):
        return OrderedDict(
            (
                (
                    quantity.name,
                    df[df[quantity.axis] > quantity.acceptable_range[1]]
                    if quantity.acceptable_range[0] is None
                    else df[~df[quantity.axis].between(*quantity.acceptable_range)],
                )
                for quantity in (
                    self.temperature,
                    self.relative_humidity,
                    self.carbon_dioxide,
                )
            )
        )

    def plot_events(self, df: DataFrame, total_events_count: int, chart: PsychroChart):
        thresholds = list(self.co2_thresholds.keys())
        assert 0 not in thresholds
        assert Series(thresholds[:-1]).is_monotonic
        assert thresholds.index(None) == len(thresholds) - 1  # the last key is None

        previous_threshold = 0
        max_co2_value = df[self.carbon_dioxide.axis].max()

        chart.plot_points_dbt_rh(
            points={
                "events_label": {
                    "label": "Events outside acceptable psychrometric and/or CO₂ concentration ranges:",
                    "style": {"marker": ""},
                    "xy": (100, 100),
                }
            }
        )

        for threshold, color in self.co2_thresholds.items():
            sub_df = (
                df[df[self.carbon_dioxide.axis] > previous_threshold]
                if threshold is None
                else df[
                    (df[self.carbon_dioxide.axis] > previous_threshold)
                    & (df[self.carbon_dioxide.axis] <= threshold)
                ]
            )
            label = "CO₂ {} ppm ({} hours ≈ {:.1f} % of occupied hours)".format(
                f"<= {threshold}"
                if previous_threshold == 0
                else f"> {previous_threshold}"
                if threshold is None
                else f"{previous_threshold} - {threshold}",
                int(len(sub_df) / self.timesteps_per_hour),
                len(sub_df) / total_events_count,
            )

            chart.plot_points_dbt_rh(
                points={
                    "": (
                        sub_df[self.temperature.axis],
                        sub_df[self.relative_humidity.axis],
                    )
                },
                scatter_style={
                    "color": color,
                    "marker": ".",
                    "sizes": [1],
                },
            )

            chart.plot_points_dbt_rh(
                points={
                    label: {
                        "label": label,
                        "style": {"color": color, "marker": ".", "markersize": 10},
                        "xy": (100, 100),
                    }
                }
            )
            if threshold is None or threshold >= max_co2_value:
                break
            else:
                previous_threshold = threshold

    def add_statistical_info(
        self, unmet_hours: dict, total_events_count: int, chart: PsychroChart
    ):
        label = "\nPercentage of unmet / occupied hours for:\n".format(
            int(total_events_count / self.timesteps_per_hour)
        ) + "\n".join(
            "- {}: {:.1f} %".format(axis_name, len(unmet_df) / total_events_count)
            for axis_name, unmet_df in unmet_hours.items()
        )
        chart.plot_points_dbt_rh(
            points={
                "stats": {
                    "label": label,
                    "style": {"marker": ""},
                    "xy": (100, 100),
                }
            }
        )

    def plot(self, df: DataFrame, filepath: str):
        chart = self.create_chart()
        ax = chart.plot()

        unmet_hours = self.filter_unmet_hours(df)
        self.plot_events(
            df=pd.concat(unmet_hours.values(), ignore_index=True).drop_duplicates()
            if self.only_unmet_hours
            else df,
            total_events_count=len(df),
            chart=chart,
        )
        self.add_statistical_info(
            unmet_hours=unmet_hours,
            total_events_count=len(df),
            chart=chart,
        )

        # Add a legend and save to file
        chart.plot_legend(markerscale=0.7, frameon=False, fontsize=10, labelspacing=1.2)
        figure = ax.get_figure()
        figure.tight_layout()
        figure.savefig(filepath)
        print(f"Saved to {filepath}")

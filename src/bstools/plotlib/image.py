# -*- coding: utf-8 -*-

from dataclasses import dataclass
from enum import Enum

from PIL import Image, ImageChops


class Matrix:
    # will be defined later when the class methods are accessible
    Rotation: Enum = NotImplemented

    @classmethod
    def clockwise_rotate(cls, values: list) -> list:
        """Clockwise rotate a 2D array from 90°."""
        return list(zip(*values[::-1]))

    @classmethod
    def counter_clockwise_rotate(cls, values: list) -> list:
        """Counter-clockwise rotate a 2D array from 90°."""
        return cls.clockwise_rotate(cls.clockwise_rotate(cls.clockwise_rotate(values)))


class __Rotation(Enum):
    clockwise = Matrix.clockwise_rotate
    counter_clockwise = Matrix.counter_clockwise_rotate


Matrix.Rotation = __Rotation


@dataclass
class Dimension:
    x: float
    y: float

    def to_tuple(self):
        return (self.x, self.y)


@dataclass
class Background:
    path: str
    offsets: Dimension


class PNGImage:
    @classmethod
    def change_pixels_alpha(cls, img: Image, white_only: bool, new_alpha: int):
        data = img.load()
        width, height = img.size
        for y in range(height):
            for x in range(width):
                if white_only:
                    if data[x, y] == (255, 255, 255, 255):
                        data[x, y] = (255, 255, 255, new_alpha)
                else:
                    r, g, b, _a = data[x, y]
                    data[x, y] = (r, g, b, new_alpha)

    @classmethod
    def overlay_background(cls, overlay_filename: str, background: Background):
        background_image = Image.open(background.path).convert("RGBA")
        overlay_image = Image.open(overlay_filename).convert("RGBA")

        cls.change_pixels_alpha(overlay_image, True, 0)
        cls.change_pixels_alpha(background_image, False, 64)

        offsets = background.offsets
        new_width = max(background_image.size[0], offsets.x + overlay_image.size[0])
        new_height = max(background_image.size[1], offsets.y + overlay_image.size[1])
        new = Image.new("RGBA", (new_width, new_height), "WHITE")
        new.paste(background_image, (0, 0))
        new.paste(overlay_image, offsets.to_tuple(), overlay_image)
        new.save(overlay_filename, "PNG")
        print(f"Saved to {overlay_filename} (added background)")

    @classmethod
    def equal(cls, path_1: str, path_2: str) -> bool:
        return (
            ImageChops.difference(Image.open(path_1), Image.open(path_2)).getbbox()
            is None
        )

# -*- coding: utf-8 -*-

from pathlib import Path

from pandas.core.frame import DataFrame

from ..lib.os import OSTools


class Processor:
    """Process any kind of data"""

    settings = NotImplemented

    def __init__(
        self,
        output_subdirs: list = None,
        overwrite: bool = False,
    ):
        self.input_dir = self.settings.input_dir
        self.output_dir = self.settings.output_dir
        if output_subdirs:
            for subdir in output_subdirs:
                self.output_dir /= subdir

        print(f"\nOutput directory is {self.output_dir}")
        if overwrite:
            OSTools.create_or_clean_directory(self.output_dir)
        else:
            OSTools.create_directory(self.output_dir)

    def process_data(self, data: DataFrame, *args, **kwargs):
        raise NotImplementedError

    def process(self, *args, **kwargs):
        raise NotImplementedError


class FileProcessor(Processor):
    """Process a file"""

    def get_input_filepath(self, filename: str) -> str:
        return self.input_dir / filename

    def parse_input_file(self, filepath: Path):
        raise NotImplementedError

    def process(self, *args, **kwargs):
        filepath = self.get_input_filepath(self.settings.filename)

        print(f"Parsing {filepath}")
        data = self.parse_input_file(filepath)

        self.process_data(data, *args, **kwargs)


class ManyFilesProcessor(Processor):
    """Process several files at once"""

    def get_input_filepaths(self, *args, **kwargs) -> list:
        raise NotImplementedError

    def parse_input_files(self, filepaths: list, *args, **kwargs):
        raise NotImplementedError

    def process(self, *args, **kwargs):
        filepaths = self.get_input_filepaths(*args, **kwargs)
        if not filepaths:
            raise Exception("Nothing to process - missing input files")

        data = self.parse_input_files(filepaths, *args, **kwargs)

        self.process_data(data, *args, **kwargs)

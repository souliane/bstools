# -*- coding: utf-8 -*-

import re
import warnings
from collections import OrderedDict
from dataclasses import dataclass
from datetime import datetime, time
from functools import cached_property
from itertools import chain
from pathlib import Path
from statistics import mean
from typing import Dict, List
from unittest.mock import patch

import pandas as pd
from dateutil.relativedelta import relativedelta
from eppy.bunch_subclass import EpBunch

from ..settings import Settings, project_path
from .data import DynamicRange, FixedRanges, ScheduleRanges
from .editor import Editor, NoZoneListDefinition, TooLessObjects

# dirty workaround for https://github.com/santoshphilip/eppy/issues/361
warnings.filterwarnings("ignore", category=DeprecationWarning)

from eppy.modeleditor import IDF  # isort:skip


def get_idd_path():
    path = project_path / "Energy+.idd"
    return (
        path if path.exists() else Settings.Energyplus.installation_path / "Energy+.idd"
    )


class IDFEditor(Editor):
    idd_path = get_idd_path()

    def __init__(self, idf_path: Path = None):
        self.idf_path = idf_path or Settings.Energyplus.idf_path

        super().__init__()

    @cached_property
    def objects(self):
        return self.idf.idfobjects

    def load(self):
        IDF.setiddname(str(self.idd_path))
        if not self.idf_path.exists():
            self.idf_path.touch()

        # dirty workaround for https://github.com/santoshphilip/eppy/issues/360
        with patch("eppy.idfreader.iddversiontuple", return_value=(9, 4, 0)):
            self.idf = IDF(str(self.idf_path))

    def save(self):
        self.idf.save()
        return self

    def get_object(self, obj_type, attribute, value) -> EpBunch:
        return self.get(self.objects[obj_type], attribute.replace(" ", "_"), value)

    def delete_object(self, obj_type: str, attribute: str, value: str):
        """Return the list representation of the deleted object or None"""
        try:
            obj = self.get(self.objects[obj_type], attribute, value)
        except TooLessObjects:
            return None
        else:
            self.idf.removeidfobject(obj)
            return obj.obj

    def delete_objects(self, obj_type):
        self.objects[obj_type].clear()

    def _set_attribute(self, obj, attribute, value_pattern, reference=None):
        setattr(
            obj,
            attribute.replace(" ", "_"),
            # TODO: we should not ignore the hint given in id_pattern!
            re.sub(r"{.+}", "{}", str(value_pattern)).format(reference),
        )

    def set_attribute(
        self,
        obj_type,
        attribute,
        value_pattern,
        obj_filter=None,
        default="",
        get_reference=None,
    ):
        for obj in self.objects[obj_type]:
            if obj_filter is None or obj_filter(obj):
                reference = get_reference(obj) if get_reference else None
                self._set_attribute(obj, attribute, value_pattern, reference)
            else:
                self._set_attribute(obj, attribute, default)
        return self

    def set_attributes(self, obj_type: str, defs: dict, get_reference=None):
        for obj in self.objects[obj_type]:
            reference = get_reference(obj) if get_reference else None
            for attribute, value_pattern in defs.items():
                self._set_attribute(obj, attribute, value_pattern, reference)
        return self

    def create_object(
        self,
        obj_type: str,
        id_reference: str = None,
        id_attrname: str = "Name",
        id_pattern: str = "{}",
        attributes: dict = None,
    ):
        attributes = attributes or {}

        if id_pattern is not None:
            id_attrname = id_attrname.replace(" ", "_")
            # TODO: we should not ignore the hint given in id_pattern!
            identifier = re.sub(r"{.+}", "{}", id_pattern).format(id_reference)
            try:
                obj = self.get_object(obj_type, id_attrname, identifier)
            except TooLessObjects:
                obj = self.idf.newidfobject(obj_type, **{id_attrname: identifier})
        else:
            obj = self.idf.newidfobject(obj_type)

        for attribute, value_pattern in attributes.items():
            self._set_attribute(obj, attribute, value_pattern, reference=id_reference)

        return obj

    def create_objects(
        self,
        obj_type: str,
        references: list,
        id_attrname: str = "Name",
        id_pattern: str = "{}",
        attributes: dict = None,
    ):
        for reference in references:
            self.create_object(
                obj_type,
                id_reference=reference,
                id_attrname=id_attrname,
                id_pattern=id_pattern,
                attributes=attributes,
            )

        assert len(self.objects[obj_type]) == len(references)
        return self

    def reset_schedule_minutes_values(self, schedule_obj, on_minutes=None):
        """Set "1" for all "Until" rules for which the minute info is in on_minutes"""
        on_minutes = on_minutes or []
        obj = schedule_obj.obj
        next_value = None
        for index, value in enumerate(obj):
            if next_value:
                obj[index], next_value = next_value, None
            else:
                match = re.search(r"Until:\s*\d{2}:(\d{2})", value)
                if match:
                    next_value = (
                        "1" if int(self.get(match.groups())) in on_minutes else "0"
                    )

    def create_constant_schedule(self, name: str, value: str):
        self.delete_object("Schedule:Compact", "Name", name)
        obj = self.create_object("Schedule:Compact", name)
        obj.Schedule_Type_Limits_Name = "Any Number"
        obj.obj.extend(["Through: 12/31", "For: AllDays", "Until: 24:00", value])

    def change_schedule_non_zero_values(self, obj, type_limits_name: str, value: str):
        obj.Schedule_Type_Limits_Name = type_limits_name
        # start with 6 because the previous fields are always not numeric
        for index, current_value in enumerate(obj.obj[6:], 6):
            try:
                if float(current_value) == 0.0:
                    obj.obj[index] = value
            except ValueError:  # no numeric field
                pass
        return obj

    def duplicate_object(self, obj_type: str, obj_name: str, new_name: str):
        source_obj = self.get_object(obj_type, "Name", obj_name)
        obj = self.idf.copyidfobject(source_obj)
        obj.Name = new_name
        return obj

    def duplicate_single_object(self, obj_type, references: list):
        objs = self.objects[obj_type]
        source_obj = self.get(objs)
        base_name, source_reference = re.match(
            r"([^ ]+) (.+)", source_obj.Name
        ).groups()
        assert source_reference in references, (
            "Could not find the source reference in the passed references, but this is "
            "our only way to validate the name pattern for new objects."
        )
        for reference in references:
            if source_reference != reference:
                obj = self.idf.copyidfobject(source_obj)
                obj.Name = f"{base_name} {reference}"
        assert len(objs) == len(references)
        return self

    def get_zone_names(self, space_types: list) -> list:
        """Return the names of the zones that belongs to one of the given space types"""
        objects = self.idf.idfobjects["ZoneList"]
        if not objects:
            raise NoZoneListDefinition
        else:
            return sorted(
                chain(*[obj.obj[2:] for obj in objects if obj.Name in space_types])
            )

    def get_outdoor_windows_of_zones(
        self, zone_names: List[str]
    ) -> Dict[str, Dict[float, List[EpBunch]]]:
        """Returns a dict of dict of list {zone_names: {azimuths: [windows]}."""
        return {
            zone_name: {
                outdoor_wall.true_azimuth: [
                    sub_surface
                    for sub_surface in outdoor_wall.subsurfaces
                    if sub_surface.Surface_Type == "Window"
                ]
                for outdoor_wall in [
                    surface
                    for surface in self.get_object(
                        "Zone", "Name", zone_name
                    ).zonesurfaces
                    if surface.Outside_Boundary_Condition == "Outdoors"
                    and surface.Surface_Type == "Wall"
                ]
            }
            for zone_name in zone_names
        }

    def search_idd(self, pattern, verbose=False):
        # TODO: performance improvement with re.finditer directly on the IDD file
        print(f"Searching for '{pattern}':")
        pattern = pattern.replace(" ", r"\s?")
        re_pattern = re.compile(f".*{pattern}.*", re.IGNORECASE)

        def print_result(name, description, level=0):
            spaces = "  " * level
            newline = "\n" if verbose else ""
            print(f"{newline}{spaces}{name}")
            if verbose and description:
                spaces += "  "
                print("{}{}".format(spaces, f"\n{spaces}".join(description)))

        def match_line(line):
            return re.match(re_pattern, line)

        def match_lines(lines):
            return any(match_line(line) for line in lines)

        for obj_def in self.idf.idd_info:
            obj_type, obj_desc = obj_def[0]["idfobj"], obj_def[0].get("memo", [])
            obj_matched = match_line(obj_type) or match_lines(obj_desc)
            if obj_matched:
                print_result(obj_type, obj_desc)
            for field_def in obj_def[1:]:
                field_name, field_desc = field_def["field"][0], field_def.get(
                    "note", []
                )
                if match_line(field_name) or match_lines(field_desc):
                    if not obj_matched:
                        print_result(obj_type, obj_desc)
                        obj_matched = True
                    print_result(field_name, field_desc, level=1)

    def list_hvac_templates(self) -> str:
        return [
            name
            for name in [obj_def[0]["idfobj"] for obj_def in self.idf.idd_info]
            if name.startswith("HVACTemplate:")
        ]


class OnOffSchedule:
    @classmethod
    def get_other_day(cls, day: str, days: int):
        return (datetime.strptime(day, "%m/%d") + relativedelta(days=days)).strftime(
            "%m/%d"
        )

    @classmethod
    def get_day_before(cls, day: str):
        return cls.get_other_day(day, days=-1)

    @classmethod
    def get_day_after(cls, day: str):
        return cls.get_other_day(day, days=+1)

    @classmethod
    def format_until_time(cls, until_time):
        return (
            f"Until: {until_time}"
            if isinstance(until_time, str)
            else "Until: 24:00"
            if until_time.hour == 0 and until_time.minute == 0
            else f"Until: {until_time.hour:02}:{until_time.minute:02}"
        )

    @classmethod
    def create(
        cls,
        editor: IDFEditor,
        name: str,
        time_ranges: Dict[tuple, ScheduleRanges] = None,
        **kwargs,
    ):
        editor.delete_object("Schedule:Compact", "Name", name)
        obj = editor.create_object("Schedule:Compact", name)
        obj.Schedule_Type_Limits_Name = "On/Off"
        current_day = "01/01"

        for date_range, values in time_ranges.items():
            starting_day, ending_day = date_range
            if starting_day != current_day:
                day_before = cls.get_day_before(starting_day)
                obj.obj.extend(
                    [f"Through: {day_before}", "For: AllDays", "Until: 24:00", "0"]
                )

            obj.obj.append(f"Through: {ending_day}")
            obj.obj.extend(cls.get_through_lines(values, **kwargs))

            current_day = cls.get_day_after(ending_day)

        if current_day != "01/01":
            obj.obj.extend([f"Through: 12/31", "For: AllDays", "Until: 24:00", "0"])

        return obj

    @classmethod
    def get_through_lines(cls, values: ScheduleRanges, **kwargs):
        raise NotImplementedError


class FixedOnOffSchedule(OnOffSchedule):
    @classmethod
    def get_through_lines(cls, values: FixedRanges):
        through_lines = []
        through_lines.extend(
            [
                "For: Holidays Weekends",
                "Until: 24:00",
                "0",
            ]
        )

        for days, ranges in values.items():
            through_lines.append(f"For: {days}")
            for hours in ranges.on_hours:
                for time in pd.date_range(hours.start, hours.end, freq="H"):
                    for minutes in ranges.on_minutes:
                        through_lines.extend(
                            [
                                f"Until: {time.hour:02}:{minutes.start}",
                                "0",
                                f"Until: {time.hour:02}:{minutes.end}",
                                "1",
                            ]
                        )
            through_lines.extend(["Until: 24:00", "0"])

        through_lines.extend(["For: AllOtherDays", "Until: 24:00", "0"])
        return through_lines


class DynamicOnOffSchedule(OnOffSchedule):
    @classmethod
    def get_through_lines(cls, values: DynamicRange, occupancy_schedule: EpBunch):
        step, duration = values.step, values.duration
        assert step > duration
        assert occupancy_schedule.obj[3] == "Through: 12/31"

        previous_line, through_lines = None, []
        from_time, until_time = None, pd.Timestamp("00:00")

        for line in occupancy_schedule.obj[4:]:
            try:
                value = float(line)
            except ValueError:
                until_match = re.search(r"Until:?\s*(\d{2}:\d{2})", line)
                if until_match:
                    until_value = until_match.groups()[0]
                    from_time = until_time
                    try:
                        until_time = pd.Timestamp(until_value)
                    except ValueError:
                        assert until_value == "24:00"
                        until_time = pd.Timestamp("00:00") + pd.Timedelta("1day")
                else:
                    assert line.startswith("For: ")
                    through_lines.append(line)
            else:
                if value > 0:
                    for index, step_time in enumerate(
                        pd.date_range(from_time, until_time, freq=f"{step}min")
                    ):
                        if index > 0:
                            through_lines.extend(
                                [cls.format_until_time(step_time), "0"]
                            )
                        tmp_time = step_time + pd.Timedelta(f"{duration}min")
                        if tmp_time > until_time:
                            tmp_time = until_time
                        through_lines.extend([cls.format_until_time(tmp_time), "1"])
                    if until_time > tmp_time:
                        through_lines.extend([cls.format_until_time(until_time), "0"])
                else:
                    through_lines.extend([previous_line, line])
            finally:
                previous_line = line

        return through_lines


@dataclass
class Zones:
    types: list
    occupancies: dict

    @dataclass
    class Occupancy:
        people: EpBunch
        schedule: EpBunch

        @classmethod
        def build(cls, editor: IDFEditor, zone_name: str):
            try:
                people = editor.get_object("People", "Zone or ZoneList Name", zone_name)
            except TooLessObjects:
                return None
            else:
                return cls(
                    people,
                    editor.get_object(
                        "Schedule:Compact",
                        "Name",
                        people.Number_of_People_Schedule_Name,
                    ),
                )

    @classmethod
    def build(cls, space_types: list, zone_names: list, editor: IDFEditor):
        return Zones(
            space_types,
            OrderedDict(
                (
                    (zone_name, cls.Occupancy.build(editor, zone_name))
                    for zone_name in zone_names
                )
            ),
        )

    @property
    def names(self):
        return self.occupancies.keys()

    @property
    def mean_number_of_people(self):
        return mean(
            occupancy.people.Number_of_People for occupancy in self.occupancies.values()
        )

    def get_people(self, zone_name: str):
        return self.occupancies[zone_name].people

    def get_occupancy_schedule(self, zone_name: str):
        return self.occupancies[zone_name].schedule

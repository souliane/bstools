# -*- coding: utf-8 -*-

import os
import re
from collections import OrderedDict
from dataclasses import dataclass
from enum import Enum
from operator import attrgetter
from pathlib import Path
from typing import Dict, List, Optional

from ..lib.logger import step
from ..lib.tools import TooManyObjects
from ..settings import Settings
from .data import DynamicRange, ScheduleRanges
from .edit_idf import DynamicOnOffSchedule, FixedOnOffSchedule, IDFEditor, Zones
from .edit_osm import OSMEditor
from .editor import TooLessObjects, get
from .hvac_template import HVACTemplateExpander

DEBUG = os.getenv("DEBUG", False)


class _Enum(Enum):
    def __str__(self) -> str:
        return self.value


class EnumWithNoneMixin:
    def __bool__(self):
        return self != self.none


class EnumWithAttributeValueMixin(EnumWithNoneMixin):
    def __str__(self) -> str:
        return "" if self == self.none else re.sub("[a-z]", "", self.value).lower()


class ScenarioComponent:
    running_order: int = NotImplemented
    naming_order: int = NotImplemented
    type: _Enum = NotImplemented

    @classmethod
    def build_configurations(cls):
        raise NotImplemented

    def __bool__(self) -> bool:
        return bool(self.type)

    def __str__(self) -> str:
        raise NotImplemented

    def sanity_check(self):
        pass

    def clean(self, scenario: "Scenario"):
        raise NotImplemented

    def configure(self, scenario: "Scenario"):
        raise NotImplemented

    def configure_post_expansion(
        self, scenario: "Scenario", expanded_editor: IDFEditor
    ):
        pass


@dataclass
class NaturalVentilation(ScenarioComponent):
    running_order = 0
    naming_order = 1
    schedule_name = Settings.Energyplus.NaturalVentilation.schedule_name

    class Type(EnumWithNoneMixin, _Enum):
        none = ""
        windows = "W"

    class Period(_Enum):
        summer = "s"
        winter = "w"
        all_year = "y"

    type: Type = Type.windows
    flow_rate_in_ach: float = 2  # `None` means "auto calculated"

    # Only used when flow_rate_in_ach is None - it that case, it must be > 0 and <= 1
    windows_opening_ratio: float = 1

    time_ranges: Dict[Period, ScheduleRanges] = None

    # ventilation is allowed if indoor temp > outdoor temp + natural_delta_temp
    delta_temp: int = -100  # always ventilate

    @classmethod
    def build(cls, *args, period: Period = None, **kwargs):
        if period is not None:
            assert "time_ranges" not in kwargs
            kwargs["time_ranges"] = {
                period: Settings.Energyplus.NaturalVentilation.fixed_ranges
            }
        return cls(*args, **kwargs)

    @classmethod
    def build_configurations(
        cls, types: list = None, flow_rates_in_ach: list = None, periods: list = None
    ):
        for natural_ventilation in types or cls.Type:
            if natural_ventilation:
                for flow_rate_in_ach in flow_rates_in_ach or [cls.flow_rate_in_ach]:
                    for period in periods or cls.Period:
                        yield cls.build(
                            type=natural_ventilation,
                            flow_rate_in_ach=flow_rate_in_ach,
                            period=period,
                        )
            else:
                yield cls(type=natural_ventilation, flow_rate_in_ach=None)

    @property
    def expanded_time_ranges(self):
        return OrderedDict(
            sorted(
                (date_range, values)
                for period, values in self.time_ranges.items()
                for date_range in Settings.Energyplus.NaturalVentilation.DateRange[
                    period.name
                ].value
            )
        )

    @property
    def period(self):
        periods = list(self.time_ranges.keys())
        try:
            return get(periods)
        except TooManyObjects:
            assert set(self.time_ranges.keys()) == {
                self.Period.winter,
                self.Period.summer,
            }
            return self.Period.all_year

    @property
    def time_ranges_are_dynamic(self) -> bool:
        are_dynamic = [
            isinstance(value, DynamicRange) for value in self.time_ranges.values()
        ]
        assert all(are_dynamic) or not any(are_dynamic)
        return all(are_dynamic)

    def __str__(self) -> str:
        flow_rate = self.flow_rate_in_ach or "a"  # "a" means "auto"
        return "" if not self else f"{self.type}{flow_rate}_{self.period}"

    def sanity_check(self):
        if self.flow_rate_in_ach is None:
            assert self.windows_opening_ratio > 0 and self.windows_opening_ratio <= 1
        else:
            assert self.flow_rate_in_ach > 0

    def clean(self, scenario: "Scenario"):
        editor = scenario.idf_editor
        editor.delete_object(
            "Schedule:Compact",
            attribute="Name",
            value=self.schedule_name,
        )
        for zone_name in scenario.ventilated_zones.names:
            editor.delete_object(
                "Schedule:Compact",
                attribute="Name",
                value=self.zone_schedule_name_pattern.format(zone_name=zone_name),
            )
        editor.delete_objects("ZoneVentilation:DesignFlowRate")
        editor.delete_objects("ZoneVentilation:WindandStackOpenArea")

    @property
    def zone_schedule_name_pattern(self) -> str:
        return f"{self.schedule_name} {{zone_name}}"

    def create_zone_dynamic_schedules(self, scenario: "Scenario"):
        """Create a dynamic on/off schedule for each ventilated zone."""
        for zone_name in scenario.ventilated_zones.names:
            DynamicOnOffSchedule.create(
                scenario.editor,
                name=self.zone_schedule_name_pattern.format(zone_name=zone_name),
                time_ranges=self.expanded_time_ranges,
                occupancy_schedule=scenario.ventilated_zones.get_occupancy_schedule(
                    zone_name
                ),
            )

    def create_design_flow_rates(
        self, scenario: "Scenario", references: List[str], schedule_name: str = None
    ):
        """Create a DesignFlowRate object for each entity in `references`.

        The given references can be zone names or zone list names.
        """
        scenario.editor.create_objects(
            "ZoneVentilation:DesignFlowRate",
            references=references,
            id_pattern="Natural Ventilation {reference}",
            attributes={
                "Zone or ZoneList Name": "{reference}",
                "Schedule Name": schedule_name or self.zone_schedule_name_pattern,
                "Design Flow Rate Calculation Method": "AirChanges/Hour",
                "Air Changes per Hour": self.flow_rate_in_ach,
                "Ventilation Type": "Natural",
                "Delta Temperature": self.delta_temp,
            },
        )

    def create_window_and_stack_open_areas(
        self, scenario: "Scenario", schedule_name: str = None
    ):
        """Create WindanStackOpenArea objects to model the windows of all ventilated zones.

        If several windows are on the same wall, only one object is created and the area
        of the windows are summed up, so in the end one object at most is created for each
        couple (ventilated zone, wall). No object is created if `wall` contains no windows.

        The summed-up area is then multiplied by self.windows_opening_ratio, so this
        parameter can be used to model e.g. "only half of the existing windows area are
        opened". Note that this has nothing to do with partially opening or tilting a
        window, because we assume that a opened window is systematically wide opened.
        """
        for (
            zone_name,
            windows_per_azimuth,
        ) in scenario.converted_osm_idf_editor.get_outdoor_windows_of_zones(
            scenario.ventilated_zones.names
        ).items():
            for azimuth, windows in windows_per_azimuth.items():
                if windows:
                    scenario.editor.create_object(
                        "ZoneVentilation:WindandStackOpenArea",
                        id_pattern=f"Window {zone_name} {azimuth}",
                        attributes={
                            "Zone Name": zone_name,
                            "Opening Area": sum(window.area for window in windows)
                            * self.windows_opening_ratio,
                            "Opening Area Fraction Schedule Name": schedule_name
                            or self.zone_schedule_name_pattern.format(
                                zone_name=zone_name
                            ),
                            "Effective Angle": azimuth,
                        },
                    )

    def configure(self, scenario: "Scenario"):
        if not self:
            return

        if self.type == self.Type.windows:
            if self.time_ranges_are_dynamic:  # one ventilation schedule per zone
                self.create_zone_dynamic_schedules(scenario)
                if self.flow_rate_in_ach:
                    self.create_design_flow_rates(
                        scenario, references=scenario.ventilated_zones.names
                    )
                else:  # flow rate is auto calculated
                    self.create_window_and_stack_open_areas(scenario)
            else:  # all zones share the same manual ventilation schedule
                FixedOnOffSchedule.create(
                    scenario.editor,
                    name=self.schedule_name,
                    time_ranges=self.expanded_time_ranges,
                )
                if self.flow_rate_in_ach:
                    self.create_design_flow_rates(
                        scenario,
                        references=scenario.ventilated_zones.types,
                        schedule_name=self.schedule_name,
                    )
                else:  # flow rate is auto calculated
                    self.create_window_and_stack_open_areas(
                        scenario, schedule_name=self.schedule_name
                    )
        else:
            raise NotImplementedError(self.type)


@dataclass
class MechanicalVentilation(ScenarioComponent):
    running_order = 1
    naming_order = 2

    schedule_name = Settings.Energyplus.MechanicalVentilation.schedule_name
    night_cycle_schedule_name = (
        Settings.Energyplus.MechanicalVentilation.night_cycle_schedule_name
    )

    class Type(EnumWithNoneMixin, _Enum):
        none = ""
        centralized = "C"
        decentralized = "D"

    class AirEconomizer(EnumWithAttributeValueMixin, _Enum):
        none = "NoEconomizer"
        fixed_dry_bulb = "FixedDryBulb"
        differential_dry_bulb = "DifferentialDryBulb"
        fixed_enthalpy = "FixedEnthalpy"
        differential_enthalpy = "DifferentialEnthalpy"
        electronic_enthalpy = "ElectronicEnthalpy"
        fixed_dew_point_and_dry_bulb = "FixedDewPointAndDryBulb"
        differential_dry_bulb_and_enthalpy = "DifferentialDryBulbAndEnthalpy"

    class NightCycle(EnumWithAttributeValueMixin, _Enum):
        none = "StayOff"
        cycle_on_any = "CycleOnAny"
        cycle_on_control_zone = "CycleOnControlZone"

    class HeatRecovery(EnumWithAttributeValueMixin, _Enum):
        none = "None"
        sensible = "Sensible"
        enthalpy = "Enthalpy"

    class HeatRecoveryExchanger(EnumWithAttributeValueMixin, _Enum):
        plate = "Plate"
        rotary = "Rotary"

    type: Type = Type.decentralized
    flow_rate_per_person: float = 10  # in L/s/person
    co2_setpoint: int = None  # in ppm

    air_economizer: AirEconomizer = AirEconomizer.none
    night_cycle: NightCycle = NightCycle.none
    heat_recovery: HeatRecovery = HeatRecovery.none
    heat_recovery_exchanger: HeatRecoveryExchanger = HeatRecoveryExchanger.plate
    heat_recovery_effectiveness: float = 0.84  # between 0 and 1

    fan_maximum_flow_rate: str = "0.21"  # in m3/s or "autosize"
    electric_strip_capacity: str = "2500"  # in W or "autosize"

    # Between 0 and 1 or "autosize". Values below 0.5 will most probably cause
    # thousands of warnings. I haven't been lucky with "autosize" so I came up
    # with an arbitrary value that works more or less for my specific use case.
    vav_minimum_air_flow_fraction: float = 0.52

    @classmethod
    def build_configurations(
        cls,
        types: list = None,
        flow_rates_per_person: list = None,
        air_economizers: list = None,
        night_cycles: list = None,
        heat_recoveries: list = None,
    ):
        for mechanical_ventilation in types or cls.Type:
            if mechanical_ventilation:
                for air_economizer in air_economizers or cls.AirEconomizer:
                    for night_cycle in night_cycles or cls.NightCycle:
                        for heat_recovery in heat_recoveries or cls.HeatRecovery:
                            for flow_rate_per_person in flow_rates_per_person or [
                                cls.flow_rate_per_person
                            ]:
                                yield cls(
                                    type=mechanical_ventilation,
                                    flow_rate_per_person=flow_rate_per_person,
                                    air_economizer=air_economizer,
                                    night_cycle=night_cycle,
                                    heat_recovery=heat_recovery,
                                )
            else:
                yield cls(type=mechanical_ventilation, flow_rate_per_person=0)

    def __str__(self) -> str:
        if not self:
            return ""
        else:
            features = "_".join(
                [str(feature) for feature in self.features if bool(feature)]
            )
            features = f"_{features}" if features else ""
            value = self.flow_rate_per_person or f"d{self.co2_setpoint}"
            return f"{self.type}{value}{features}"

    @property
    def features(self) -> list:
        return [self.heat_recovery, self.air_economizer, self.night_cycle]

    @property
    def supply_fan_placement(self):
        return (
            {
                # TODO: is this the right default for a centralised system?
                self.Type.centralized: "DrawThrough",
                self.Type.decentralized: "BlowThrough",
            }[self.type]
            if self
            else ""
        )

    def get_fan_minimum_flow_rate(self, scenario, zone_name: str):
        nominal_rate = self.get_fan_nominal_flow_rate(scenario, zone_name)
        return (
            "autosize"
            if "autosize" in (nominal_rate, self.vav_minimum_air_flow_fraction)
            else nominal_rate * float(self.vav_minimum_air_flow_fraction)
        )

    def get_fan_nominal_flow_rate(self, scenario, zone_name: str):
        return (
            "autosize"
            if (
                self.fan_maximum_flow_rate == "autosize"
                or self.flow_rate_per_person is None
            )
            else min(
                float(self.fan_maximum_flow_rate),
                self.flow_rate_per_person
                / 1000
                * scenario.ventilated_zones.get_people(zone_name).Number_of_People,
            )
        )

    def sanity_check(self):
        if self.co2_setpoint:
            assert float(self.co2_setpoint) > 0, "CO2 setpoint must be positive"
            assert not self.flow_rate_per_person
            assert float(self.fan_maximum_flow_rate) > 0
        else:
            assert float(self.flow_rate_per_person) > 0

        if self.heat_recovery == self.HeatRecovery.sensible:
            assert self.heat_recovery_exchanger == self.HeatRecoveryExchanger.plate
        elif self.heat_recovery == self.HeatRecovery.enthalpy:
            assert self.heat_recovery_exchanger == self.HeatRecoveryExchanger.rotary

    def clean(self, scenario: "Scenario"):
        scenario.idf_editor.delete_objects("HVACTemplate:System:DedicatedOutdoorAir")

    def configure(self, scenario: "Scenario"):
        if not self:
            return

        if self.type == self.Type.decentralized:
            for zone_name in scenario.ventilated_zones.names:
                heating_attributes = (
                    {
                        "Heating Coil Type": "Electric",
                        "Heating Coil Availability Schedule Name": scenario.heating.schedule_name,
                        "Heating Coil Setpoint Control Type": "Scheduled",
                        "Heating Coil Setpoint Schedule Name": scenario.heating.heating_setpoint_schedule_name,
                    }
                    if (
                        zone_name in scenario.heated_zones.names
                        and scenario.heating.type.use_electric_strip
                    )
                    else {"Heating Coil Type": "None"}
                )

                scenario.editor.create_object(
                    "HVACTemplate:System:DedicatedOutdoorAir",
                    id_reference=zone_name,
                    id_pattern="System Template {zone_name}",
                    attributes={
                        "System Availability Schedule Name": self.schedule_name,
                        "Supply Fan Placement": self.supply_fan_placement,
                        "Supply Fan Flow Rate": self.get_fan_nominal_flow_rate(
                            scenario, zone_name
                        ),
                        "Cooling Coil Type": "None",
                        # May not be needed, but in case it affects the air economiser...
                        "Cooling Coil Setpoint Control Type": "OutdoorAirTemperatureReset",
                        # Expand with heat recovery even if it's disabled to ease other
                        # features configuration - we will disable it later if needed.
                        "Heat Recovery Type": self.heat_recovery.value
                        if self.heat_recovery
                        else self.HeatRecovery.sensible.value,
                        "Heat Recovery Heat Exchanger Type": self.heat_recovery_exchanger.value,
                        "Heat Recovery Sensible Effectiveness": self.heat_recovery_effectiveness,
                        "Heat Recovery Latent Effectiveness": self.heat_recovery_effectiveness,
                        **heating_attributes,
                    },
                )
        else:
            raise NotImplementedError(self.type)

    def configure_post_expansion(
        self, scenario: "Scenario", expanded_editor: IDFEditor
    ):
        if not self:
            return

        self._post_configure_sizing(scenario, expanded_editor)
        self._post_configure_outdoor_air(scenario, expanded_editor)
        self._post_configure_economiser(scenario, expanded_editor)
        self._post_configure_night_cycle(scenario, expanded_editor)
        self._post_configure_recovery(scenario, expanded_editor)
        self._post_configure_strips(scenario, expanded_editor)

        if self.co2_setpoint:
            self._post_configure_dcv(scenario, expanded_editor)

    def _post_configure_sizing(self, scenario, expanded_editor):

        # remove some annoying warnings, but we don't cool anyway
        expanded_editor.set_attributes(
            "Sizing:Zone",
            {
                "Zone Cooling Design Supply Air Temperature Input Method": "TemperatureDifference",
                "Zone Cooling Design Supply Air Temperature Difference": 5,
            },
        )

        for zone_name in scenario.ventilated_zones.names:
            sizing = expanded_editor.get_object(
                "Sizing:Zone", "Zone or ZoneList Name", zone_name
            )
            # Effects: almost not noticeable (maybe a little more CO2 unmet
            # hours and little less consumption) but it sounds right.
            sizing.Account_for_Dedicated_Outdoor_Air_System = "Yes"

        for zone_name in scenario.ventilated_zones.names:
            obj = expanded_editor.get_object(
                "Sizing:System", "AirLoop Name", f"System Template {zone_name}"
            )
            obj.Cooling_Supply_Air_Flow_Rate_Method = "DesignDay"
            obj.Cooling_Supply_Air_Flow_Rate = 0
            obj.Cooling_Design_Capacity_Method = "None"
            obj.System_Outdoor_Air_Method = "ZoneSum"
            # Effects: use settings from DesignSpecification:OutdoorAir
            obj.Design_Outdoor_Air_Flow_Rate = "autosize"

    def _post_configure_outdoor_air(self, scenario, expanded_editor):
        expanded_editor.set_attributes(
            "DesignSpecification:OutdoorAir",
            {
                "Outdoor Air Schedule Name": "{}",
                # Effects: if uncommented, this will disable mechanical ventilation
                # "Outdoor Air Method": "ProportionalControlBasedonOccupancySchedule",
            },
            get_reference=lambda obj: scenario.ventilated_zones.get_people(
                re.match("SZ DSOA (.*)", obj.Name).group(1)
            ).Number_of_People_Schedule_Name,
        )

        for zone_name in scenario.ventilated_zones.names:
            obj = expanded_editor.get_object(
                "Controller:OutdoorAir",
                "Name",
                f"System Template {zone_name} OA Controller",
            )
            obj.Maximum_Outdoor_Air_Flow_Rate = self.fan_maximum_flow_rate
            obj.Minimum_Limit_Type = "ProportionalMinimum"
            # Effects: this value can badly limit the overall ventilation of
            # some configurations, just as if it was the maximum flow rate,
            # so be careful when you change something. When auto-sized,
            # it will be close to the maximum flow rate, which is not good
            # for use cases that ventilate regarding the occupation.
            obj.Minimum_Outdoor_Air_Flow_Rate = self.get_fan_minimum_flow_rate(
                scenario, zone_name
            )
            # Effects: setting this will disable occupancy-based ventilation
            # "Minimum Fraction of Outdoor Air Schedule Name": "Always On Discrete",
            # Effects: setting this schedule will deactivate night cycle
            # "Minimum_Outdoor_Air_Schedule_Name": self.schedule_name,
            obj.Mechanical_Ventilation_Controller_Name = f"{zone_name} DCV Controller"

        expanded_editor.create_objects(
            "Controller:MechanicalVentilation",
            references=scenario.ventilated_zones.names,
            id_pattern="{zone_name} DCV Controller",
            attributes={
                "Availability Schedule Name": self.schedule_name,
                # Effects: none for the occupancy-based ventilation but needed
                # for the CO2-based one, so we can always leave it to "Yes".
                "Demand Controlled Ventilation": "Yes",
                "System Outdoor Air Method": "ProportionalControlBasedonOccupancySchedule",
                "Zone or ZoneList 1 Name": "{zone_name}",
                "Design Specification Outdoor Air Object Name 1": f"SZ DSOA {zone_name}",
                "Design Specification Zone Air Distribution Object Name 1": f"SZ DSZAD {zone_name}",
            },
        )

        for zone_name in scenario.ventilated_zones.names:
            minimum_rate = self.get_fan_minimum_flow_rate(scenario, zone_name)
            if minimum_rate == "autosize":
                method, fraction, schedule_name = "Constant", "autosize", ""
            else:
                method, fraction, schedule_name = (
                    "Scheduled",
                    "0",
                    f"{zone_name} VAV Minimum Air Flow Fraction",
                )
                new_schedule = expanded_editor.duplicate_object(
                    "Schedule:Compact", self.schedule_name, schedule_name
                )
                expanded_editor.change_schedule_non_zero_values(
                    new_schedule,
                    "Fraction",
                    minimum_rate / float(self.fan_maximum_flow_rate),
                )

            obj = expanded_editor.get_object(
                "AirTerminal:SingleDuct:VAV:NoReheat",
                "Name",
                f"{zone_name} DOAS Air Terminal",
            )
            obj.Maximum_Air_Flow_Rate = self.fan_maximum_flow_rate
            obj.Zone_Minimum_Air_Flow_Input_Method = method
            obj.Constant_Minimum_Air_Flow_Fraction = fraction
            obj.Minimum_Air_Flow_Fraction_Schedule_Name = schedule_name
            obj.Design_Specification_Outdoor_Air_Object_Name = f"SZ DSOA {zone_name}"

    def _post_configure_economiser(self, scenario, expanded_editor):
        # HVACTemplate:System:DedicatedOutdoorAir automatically expanded with
        # an air economiser but didn't allow to configure it, so we do it now
        expanded_editor.set_attributes(
            "Controller:OutdoorAir",
            {
                "Economizer_Control_Type": self.air_economizer.value,
                "Economizer_Control_Action_Type": "MinimumFlowWithBypass",
            },
        )

    def _post_configure_night_cycle(self, scenario, expanded_editor):
        # Transform AvailabilityManager:Scheduled to AvailabilityManager:NightCycle,
        # because HVACTemplate:System:DedicatedOutdoorAir has no night cycle feature.
        # When night cycle is disabled, we set the correspond control type but still
        # want to use AvailabilityManager:NightCycle objects to ease scenarios comparison.
        scheduled_managers = expanded_editor.objects["AvailabilityManager:Scheduled"]
        assert len(scheduled_managers) == len(
            scenario.ventilated_zones.names
        ), "Found {} AvailabilityManager:Scheduled instead of {}".format(
            len(scheduled_managers), len(scenario.ventilated_zones.names)
        )
        expanded_editor.delete_objects("AvailabilityManager:Scheduled")
        expanded_editor.create_objects(
            "AvailabilityManager:NightCycle",
            references=scenario.ventilated_zones.names,
            id_pattern="System Template {zone_name} Availability",
            attributes={
                "Applicability Schedule Name": self.night_cycle_schedule_name,
                "Fan Schedule Name": self.schedule_name,
                "Control Type": self.night_cycle.value,
                "Cycling Run Time Control Type": "Thermostat",
                "Control Zone or Zone List Name": "{zone_name}",
            },
        )
        expanded_editor.set_attribute(
            "AvailabilityManagerAssignmentList",
            "Availability Manager 1 Object Type",
            "AvailabilityManager:NightCycle",
        )

    def _post_configure_recovery(self, scenario, expanded_editor):
        # Fix or disable heat recovery
        for zone_name in scenario.ventilated_zones.names:
            obj = expanded_editor.get_object(
                "HeatExchanger:AirToAir:SensibleAndLatent",
                "Name",
                f"System Template {zone_name} Heat Recovery",
            )
            obj.Availability_Schedule_Name = (
                self.schedule_name if self.heat_recovery else "Always Off Discrete"
            )
            # Effects: almost not noticeable but consumes a little less than
            # when using value from self.get_fan_nominal_flow_rate(...)
            obj.Nominal_Supply_Air_Flow_Rate = "autosize"

    def _post_configure_strips(self, scenario: "Scenario", expanded_editor: IDFEditor):
        expanded_editor.set_attribute(
            "Coil:Heating:Electric",
            "Nominal Capacity",
            self.electric_strip_capacity,
        )

    def _post_configure_dcv(self, scenario: "Scenario", expanded_editor: IDFEditor):
        assert self.co2_setpoint

        expanded_editor.set_attributes(
            "Controller:MechanicalVentilation",
            {
                "Demand Controlled Ventilation": "Yes",
                "System Outdoor Air Method": "IndoorAirQualityProcedure",
            },
        )

        expanded_editor.create_constant_schedule("CO2 Setpoint", self.co2_setpoint)
        expanded_editor.create_objects(
            "ZoneControl:ContaminantController",
            references=scenario.ventilated_zones.names,
            id_pattern="{zone_name} CO2 Controller",
            attributes={
                "Zone Name": "{zone_name}",
                "Carbon Dioxide Setpoint Schedule Name": "CO2 Setpoint",
            },
        )


@dataclass
class Heating(ScenarioComponent):
    running_order = 2
    naming_order = 0

    schedule_name = Settings.Energyplus.Heating.schedule_name
    heating_setpoint_schedule_name = (
        Settings.Energyplus.Heating.heating_setpoint_schedule_name
    )
    cooling_setpoint_schedule_name = (
        Settings.Energyplus.Heating.cooling_setpoint_schedule_name
    )
    thermostat_name = Settings.Energyplus.Heating.thermostat_name

    class Type(EnumWithNoneMixin, _Enum):
        none = ""
        district = "D"
        electric_strip = "S"
        district_and_electric_strip = "DS"

        @property
        def use_district(self) -> bool:
            return "D" in self.value

        @property
        def use_electric_strip(self) -> bool:
            return "S" in self.value

    type: Type = Type.district

    @classmethod
    def build_configurations(cls, types: list = None):
        for heating in types or cls.Type:
            yield cls(type=heating)

    def __str__(self) -> str:
        return str(self.type)

    def clean(self, scenario: "Scenario"):
        editor = scenario.idf_editor
        editor.delete_objects("HVACTemplate:Thermostat")
        editor.delete_objects("HVACTemplate:Plant:HotWaterLoop")
        editor.delete_objects("HVACTemplate:Plant:Boiler")
        editor.delete_objects("HVACTemplate:Zone:BaseboardHeat")
        editor.delete_objects("DesignSpecification:OutdoorAir")

    def configure(self, scenario: "Scenario"):
        if not self:
            return

        scenario.editor.create_object(
            "HVACTemplate:Thermostat",
            self.thermostat_name,
            attributes={
                "Heating Setpoint Schedule Name": self.heating_setpoint_schedule_name,
                "Cooling Setpoint Schedule Name": self.cooling_setpoint_schedule_name,
            },
        )
        scenario.editor.create_object(
            "HVACTemplate:Plant:HotWaterLoop",
            "Heating Plant Loop",
            attributes={"Pump Schedule Name": self.schedule_name},
        )
        scenario.editor.create_object(
            "HVACTemplate:Plant:Boiler",
            "District Heating",
            attributes={"Boiler Type": "DistrictHotWater"},
        )

        for zone_name in scenario.heated_zones.names:
            method, dsoa_name, system_name, flow_rate = "Flow/Person", "", "", 0
            if (
                zone_name in scenario.ventilated_zones.names
                and scenario.mechanical_ventilation.type
                == MechanicalVentilation.Type.decentralized
            ):
                system_name = f"System Template {zone_name}"
                if scenario.mechanical_ventilation.co2_setpoint:
                    method = "DetailedSpecification"
                    dsoa_name = f"SZ DSOA {zone_name}"
                    scenario.editor.create_object(
                        "DesignSpecification:OutdoorAir",
                        id_pattern=dsoa_name,
                        attributes={
                            "Outdoor Air Method": "IndoorAirQualityProcedure",
                        },
                    )
                else:
                    flow_rate = (
                        scenario.mechanical_ventilation.flow_rate_per_person / 1000
                    )

            scenario.editor.create_object(
                "HVACTemplate:Zone:BaseboardHeat",
                id_reference=zone_name,
                id_attrname="Zone Name",
                attributes={
                    "Baseboard Heating Availability Schedule Name": self.schedule_name
                    if self.type.use_district
                    else "Always Off Discrete",
                    "Template Thermostat Name": self.thermostat_name,
                    "Baseboard Heating Type": "HotWater",
                    "Dedicated Outdoor Air System Name": system_name,
                    "Outdoor Air Method": method,
                    "Outdoor Air Flow Rate per Person": flow_rate,
                    "Design_Specification_Outdoor_Air_Object_name": dsoa_name,
                },
            )


class Scenario:
    components: List[ScenarioComponent] = NotImplemented
    number_of_people_fraction: Optional[float] = None

    def __init__(
        self,
        idf_editor: IDFEditor,
        osm_editor: OSMEditor,
        converted_osm_idf_editor: IDFEditor,
        heated_zones: Zones,
        ventilated_zones: Zones,
        number_of_people_fraction: float = None,
    ):
        self.idf_editor = idf_editor
        self.osm_editor = osm_editor
        self.converted_osm_idf_editor = converted_osm_idf_editor
        self.heated_zones = heated_zones
        self.ventilated_zones = ventilated_zones

        for cls in [NaturalVentilation, MechanicalVentilation, Heating]:
            try:
                self.get_component(cls)
            except TooLessObjects:
                self.components.append(cls(type=cls.Type.none))

        # mechanical ventilation needs to be configured before heating
        self.components.sort(key=attrgetter("running_order"))

        if number_of_people_fraction is not None:
            self.number_of_people_fraction = number_of_people_fraction

    @classmethod
    def get(cls, objs):
        assert len(objs) == 1, "Object length is {}".format(len(objs))
        return objs[0]

    @property
    def editor(self):
        return self.idf_editor

    @property
    def has_air_loops(self):
        return any(
            bool(component) and isinstance(component, MechanicalVentilation)
            for component in self.components
        )

    @property
    def heating(self):
        return self.get_component(Heating)

    @property
    def mechanical_ventilation(self):
        return self.get_component(MechanicalVentilation)

    def get_component(self, cls: ScenarioComponent):
        return get(
            [component for component in self.components if isinstance(component, cls)]
        )

    def set_osm_options(self, options: dict):
        for obj_type, values in options.items():
            for attribute, value in values.items():
                self.osm_editor.set_unique_object_attribute(
                    obj_type, attribute, str(value)
                )

    def set_common_options(self):
        self.set_osm_options(
            {
                "OS:RunPeriod": {
                    "Name": "Winter DEBUG" if DEBUG else "Year 2020",
                    "Begin Day of Month": 1,
                    "Begin Month": 1,
                    "End Day of Month": 10 if DEBUG else 31,
                    "End Month": 1 if DEBUG else 12,
                },
                "OS:SimulationControl": {
                    "Do Zone Sizing Calculation": "Yes",
                    "Do System Sizing Calculation": "Yes"
                    if self.has_air_loops
                    else "No",
                    "Do Plant Sizing Calculation": "Yes",
                    "Solar Distribution": "MinimalShadowing"
                    if DEBUG
                    else "FullExterior",
                },
                "OS:Timestep": {"Number of Timesteps per Hour": 4 if DEBUG else 12},
            }
        )
        if self.number_of_people_fraction:
            for people in self.osm_editor.get_objects("OS:People:Definition"):
                self.osm_editor.set_attribute(
                    people,
                    "Number Of People",
                    float(self.osm_editor.get_attribute(people, "Number Of People"))
                    * self.number_of_people_fraction,
                )

        if DEBUG:
            self.idf_editor.create_object(
                "RunPeriod",
                id_reference="Summer DEBUG",
                attributes={
                    "Begin Day of Month": 1,
                    "Begin Month": 6,
                    "End Day of Month": 10,
                    "End Month": 6,
                    "Day of Week for Start Day": "Monday",
                },
            )
        else:
            self.idf_editor.delete_objects("RunPeriod")

    def configure(self) -> Path:
        """
        :returns Path: the expanded IDF path
        """
        print("\n")
        step(
            f"Modifying model to fit variant {self.__class__.__name__} (DEBUG={DEBUG})"
        )

        self.set_common_options()

        # make sure to clean for *all* components before starting any configuration
        for component in self.components:
            component.sanity_check()
            component.clean(self)

        for component in self.components:
            component.configure(self)

        self.idf_editor.save()
        self.osm_editor.save(overwrite=True)

        # expand in.idf to expanded.idf
        expander = HVACTemplateExpander()
        expanded_idf_path = expander.expand_idf(Settings.Energyplus.idf_path)
        expanded_editor = IDFEditor(idf_path=expanded_idf_path)

        for component in self.components:
            component.configure_post_expansion(self, expanded_editor)

        expanded_editor.save()
        return expanded_idf_path

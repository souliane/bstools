# -*- coding: utf-8 -*-

import difflib
import os
import subprocess
import sys
from functools import cached_property
from pathlib import Path

from ..lib.logger import step
from ..lib.os import OSTools
from ..settings import Settings
from . import scenario, variants
from .edit_idf import IDFEditor, NoZoneListDefinition, Zones
from .edit_osm import OSMEditor, osm_to_idf
from .hvac_template import HVACTemplateExpander


class Workflow:
    hvac_diagram_path = Settings.Energyplus.hvac_diagram_path
    read_vars_eso_path = Settings.Energyplus.read_vars_eso_path

    def __init__(self):
        self.idf_editor = IDFEditor()
        self.osm_editor = OSMEditor()

        # We need this to load people and schedules definitions that are in the OSM file.
        # TODO: make it work when those definitions are in the IDF, or when there's no OSM.
        converted_osm_idf_path = Path("converted_osm.idf")
        osm_to_idf(
            str(Settings.Energyplus.osm_path),
            str(converted_osm_idf_path),
            overwrite=True,
        )
        self.converted_osm_idf_editor = IDFEditor(idf_path=converted_osm_idf_path)

        self.heated_zones = self.build_zones(Settings.Energyplus.heated_space_types)
        self.ventilated_zones = self.build_zones(
            Settings.Energyplus.ventilated_space_types
        )

    @classmethod
    def get_target_path_for_variant(cls, variant: str):
        return Settings.Workflow.results_path / variant

    @classmethod
    def copy_results(cls, variant: str, overwrite=False):
        target_path = cls.get_target_path_for_variant(variant)
        if target_path.exists():
            if overwrite:
                OSTools.replace(Settings.Workflow.simulation_path, target_path)
            else:
                raise Exception(f"Target path exists: {target_path}")
        else:
            OSTools.rename(Settings.Workflow.simulation_path, target_path)

        SimulationLogs().errors(variant)

        step(f"Generating HVAC loops diagram in {target_path}")
        OSTools.execute(
            str(cls.hvac_diagram_path),
            shell=True,
            cwd=target_path,
            stdout=subprocess.DEVNULL,
        )
        step(f"Converting ESO to CSV in {target_path}")
        OSTools.execute(
            # this trick with " '' N" allows to read all the CSV and not just a
            # chunk, it comes from Julien Marrec on unmethours.
            f"{cls.read_vars_eso_path} '' N",
            shell=True,
            cwd=target_path,
        )

    @classmethod
    def check_results(cls):
        with open(Settings.Workflow.error_path) as fp:
            result = fp.readlines()[-1]
        assert "EnergyPlus Completed Successfully" in result, result

    @classmethod
    def copy_variant_idf(cls, idf_path: Path, variant_name: str):
        OSTools.copy(
            idf_path,
            idf_path.parent / (idf_path.stem + f".{variant_name}" + idf_path.suffix),
        )

    @property
    def osm_path(self):
        return Settings.Energyplus.osm_path

    @cached_property
    def osm_backup_path(self):
        return self.osm_path.parent / (self.osm_path.name + ".bak")

    def build_zones(self, space_types: list):
        try:
            zone_names = self.idf_editor.get_zone_names(space_types)
        except NoZoneListDefinition:  # TODO: raise a better exception
            zone_names = self.osm_editor.get_zone_names(space_types)

        assert len(zone_names) == len(
            set(zone_names)
        ), "Zone definitions are overlapping: {zone_names}"

        return Zones.build(space_types, zone_names, self.converted_osm_idf_editor)

    def run_openstudio_workflow(self, variant_name: str, overwrite: bool):
        step(f"Running OpenStudio workflow (please be patient)...")
        OSTools.execute(
            "{} run --workflow {}".format(
                Settings.Workflow.openstudio,
                Settings.Workflow.osw_path,
            ),
            shell=True,
        )
        self.check_results()
        self.copy_results(variant_name, overwrite=overwrite)

    def process_variant(
        self, variant_name: str, skip_simulation: bool, overwrite: bool
    ):
        variant = getattr(variants, variant_name)(
            self.idf_editor,
            self.osm_editor,
            self.converted_osm_idf_editor,
            heated_zones=self.heated_zones,
            ventilated_zones=self.ventilated_zones,
        )

        expanded_idf_path = variant.configure()

        # copy the modified in.idf and expanded.idf file to keep track
        self.copy_variant_idf(Settings.Energyplus.idf_path, variant_name)
        self.copy_variant_idf(expanded_idf_path, variant_name)

        if skip_simulation:
            print(
                "IDF would be expanded to:\n  {}".format(
                    "\n  ".join(
                        HVACTemplateExpander.get_expansion_object_types(
                            Settings.Energyplus.idf_path, expanded_idf_path
                        )
                    )
                )
            )
        else:
            if Settings.Workflow.simulation_path.exists() and not overwrite:
                raise Exception(
                    f"Simulation run path exists: {Settings.Workflow.simulation_path}"
                )
            else:
                self.run_openstudio_workflow(variant_name, overwrite=overwrite)

    def run(
        self,
        variant_name: str = None,
        overwrite: bool = False,
        skip_simulation: bool = False,
        use_existing_results: bool = False,
    ):
        variant_names = variants.__all__
        if variant_name and variant_name not in variant_names:
            raise NotImplementedError(
                "Variant {} not found! Existing ones are:\n{}.".format(
                    variant_name, "\n".join(variant_names)
                )
            )
        if use_existing_results:
            assert variant_name and Settings.Workflow.simulation_path.exists(), (
                "Passing 'use_existing_results=True' only makes sense together with "
                f"'variant_name', and {Settings.Workflow.simulation_path} must exists."
            )
            self.copy_results(variant_name, overwrite=overwrite)
        else:
            if not any((skip_simulation, scenario.DEBUG)) and not variant_name:
                user_input = input(
                    "Running simulations for the whole year takes long - do you want "
                    "to shorten the period and switch to DEBUG mode now? [Y/n]"
                )
                if not user_input or user_input not in "nN":
                    scenario.DEBUG = True
                    print("Switched scenario to DEBUG mode!")

            for variant_name in [variant_name] if variant_name else variant_names:
                if (
                    self.get_target_path_for_variant(variant_name).exists()
                    and not overwrite
                ):
                    print(
                        f"Skipping {variant_name} because the target path already exists."
                    )
                else:
                    self.backup_osm_file()
                    try:
                        self.process_variant(
                            variant_name,
                            skip_simulation=skip_simulation,
                            overwrite=overwrite,
                        )
                    finally:
                        self.restore_osm_file()
                        # also reload the editor to discard changes from the variant!
                        self.osm_editor = OSMEditor()

    def backup_osm_file(self):
        if self.osm_backup_path.exists():
            raise Exception(
                f"{self.osm_backup_path} already exists! Previous workflow "
                "probably failed, please check and restore the file manually."
            )
        OSTools.copy(self.osm_path, self.osm_backup_path)

    def restore_osm_file(self):
        OSTools.replace(self.osm_backup_path, self.osm_path)


class SimulationLogs:
    error_filename = "eplusout.err"

    def get_error_files(self, variant_name: str = None) -> list:
        return [
            Settings.Workflow.results_path / dirname / self.error_filename
            for dirname in os.listdir(Settings.Workflow.results_path)
            if not dirname.startswith("_")
            and (not variant_name or dirname == variant_name)
        ]

    def errors(self, variant_name: str = None):
        with open(Settings.Workflow.ignored_error_path) as fp:
            ignored_lines = fp.readlines()

        for error_file in self.get_error_files(variant_name):
            with open(error_file) as fp:
                lines = fp.readlines()
            sys.stdout.writelines(
                difflib.unified_diff(
                    ignored_lines,
                    lines,
                    fromfile=str(Settings.Workflow.ignored_error_path),
                    tofile=str(error_file),
                    n=0,
                )
            )

# -*- coding: utf-8 -*-


import inspect
import os
import sys
from operator import attrgetter
from typing import List

from ..lib.logger import debug
from .data import DynamicRange
from .scenario import Heating, MechanicalVentilation, NaturalVentilation, Scenario

AirEconomizer = MechanicalVentilation.AirEconomizer
NightCycle = MechanicalVentilation.NightCycle
HeatRecovery = MechanicalVentilation.HeatRecovery
HeatRecoveryExchanger = MechanicalVentilation.HeatRecoveryExchanger

module = sys.modules[__name__]

DEBUG = os.getenv("DEBUG", False)


# You can define your own variants like that (here with minimal configuration):
#
#  class MyScenario(Scenario):
#     components = [
#       Heating(type=Heating.Type.district),
#       NaturalVentilation(flow_rates_in_ach=2),
#       MechanicalVentilation(type=MechanicalVentilation.Type.decentralized)
#     ]
#
# ... but if you don't, we will automatically generate a bunch of them
class Generator:
    count = 0
    friendly_name: bool = True

    @classmethod
    def generate_variant_name(cls, components: List["ScenarioComponent"]):
        name = "_".join(
            [
                str(component)
                for component in sorted(components, key=attrgetter("naming_order"))
                if component
            ]
        )
        if cls.friendly_name:
            # try to shorten the variant name using arbitrary shortcuts that might not
            # make sense to everybody... feel free to adapt this as you like, it won't
            # break the code.
            return (
                name.replace("_w", "w")  # NaturalVentilation.Period.winter
                .replace("_y", "y")  # NaturalVentilation.Period.year
                .replace(
                    "_s", "r"
                )  # MechanicalVentilation.HeatRecovery.sensible --> Recovery
                .replace(
                    "_e", "w"
                )  # MechanicalVentilation.HeatRecovery.enthalpy --> recovery Wheel
                .replace(
                    "_cocz", "n"
                )  # MechanicalVentilation.NightCycle.cycle_on_control_zone --> Night
                .replace(
                    "_ddb", "e"
                )  # MechanicalVentilation.AirEconomizer.differential_dry_bulb --> Economizer
            )
        else:
            return name

    @classmethod
    def create_variant(
        cls, components: List["ScenarioComponent"], variant_name: str = None, **kwargs
    ):
        if not variant_name:
            variant_name = cls.generate_variant_name(components)
        if DEBUG:
            debug(f"Dynamic definition of {variant_name}")
        try:
            getattr(module, variant_name)
        except AttributeError:
            setattr(
                module,
                variant_name,
                type(variant_name, (Scenario,), {"components": components, **kwargs}),
            )
        else:
            raise Exception(f"Scenario {variant_name} is already defined")

        cls.count += 1

    @classmethod
    def generate_interesting_scenarios(cls):

        # Does shock ventilation suffice to keep low CO2 concentration and temperature?
        for natural_ventilation in NaturalVentilation.build_configurations(
            flow_rates_in_ach=[2, 4],
            periods=[NaturalVentilation.Period.all_year],
        ):
            cls.create_variant(
                [Heating(type=Heating.Type.district), natural_ventilation]
            )

        # Full mechanical ventilation, with/without winter shock ventilation
        for natural_ventilation in NaturalVentilation.build_configurations(
            flow_rates_in_ach=[2],
            periods=[NaturalVentilation.Period.winter],
        ):
            for mechanical_ventilation in MechanicalVentilation.build_configurations(
                types=[MechanicalVentilation.Type.decentralized],
                flow_rates_per_person=[6, 10, 14],
                air_economizers=(AirEconomizer.differential_dry_bulb,),
                night_cycles=(NightCycle.cycle_on_control_zone,),
                heat_recoveries=(HeatRecovery.sensible,),
            ):
                cls.create_variant(
                    [
                        Heating(type=Heating.Type.district_and_electric_strip),
                        natural_ventilation,
                        mechanical_ventilation,
                    ]
                )

        # Mechanical ventilation without night cycle, with winter shock ventilation
        for mechanical_ventilation in MechanicalVentilation.build_configurations(
            types=[MechanicalVentilation.Type.decentralized],
            flow_rates_per_person=[10, 14],
            air_economizers=(AirEconomizer.differential_dry_bulb,),
            night_cycles=(NightCycle.none,),
            heat_recoveries=(HeatRecovery.sensible,),
        ):
            cls.create_variant(
                [
                    Heating(type=Heating.Type.district_and_electric_strip),
                    NaturalVentilation.build(
                        type=NaturalVentilation.Type.windows,
                        flow_rate_in_ach=2,
                        period=NaturalVentilation.Period.winter,
                    ),
                    mechanical_ventilation,
                ]
            )

        # Mechanical ventilation without electric strips, with winter shock ventilation
        for mechanical_ventilation in MechanicalVentilation.build_configurations(
            types=[MechanicalVentilation.Type.decentralized],
            flow_rates_per_person=[10, 14],
            air_economizers=(AirEconomizer.differential_dry_bulb,),
            night_cycles=(NightCycle.cycle_on_control_zone,),
            heat_recoveries=(HeatRecovery.sensible,),
        ):
            cls.create_variant(
                [
                    Heating(type=Heating.Type.district),
                    NaturalVentilation.build(
                        type=NaturalVentilation.Type.windows,
                        flow_rate_in_ach=2,
                        period=NaturalVentilation.Period.winter,
                    ),
                    mechanical_ventilation,
                ]
            )

        # Mechanical ventilation with electric strips but without heat recovery
        for mechanical_ventilation in MechanicalVentilation.build_configurations(
            types=[MechanicalVentilation.Type.decentralized],
            flow_rates_per_person=[10, 14],
            air_economizers=(AirEconomizer.differential_dry_bulb,),
            night_cycles=(NightCycle.cycle_on_control_zone,),
            heat_recoveries=(HeatRecovery.none,),
        ):
            cls.create_variant(
                [
                    Heating(type=Heating.Type.district_and_electric_strip),
                    NaturalVentilation.build(
                        type=NaturalVentilation.Type.windows,
                        flow_rate_in_ach=2,
                        period=NaturalVentilation.Period.winter,
                    ),
                    mechanical_ventilation,
                ]
            )

        # Mechanical ventilation with DCV, heat recovery via plate exchanger
        for co2_setpoint in (800, 1000):
            cls.create_variant(
                [
                    Heating(type=Heating.Type.district_and_electric_strip),
                    NaturalVentilation(type=NaturalVentilation.Type.none),
                    MechanicalVentilation(
                        type=MechanicalVentilation.Type.decentralized,
                        flow_rate_per_person=None,
                        co2_setpoint=co2_setpoint,
                        air_economizer=AirEconomizer.differential_dry_bulb,
                        night_cycle=NightCycle.cycle_on_control_zone,
                        heat_recovery=HeatRecovery.sensible,
                    ),
                ]
            )

        # Mechanical ventilation with DCV 800 ppm, energy recovery via enthalpy wheel
        cls.create_variant(
            [
                Heating(type=Heating.Type.district_and_electric_strip),
                NaturalVentilation(type=NaturalVentilation.Type.none),
                MechanicalVentilation(
                    type=MechanicalVentilation.Type.decentralized,
                    flow_rate_per_person=None,
                    co2_setpoint=800,
                    air_economizer=AirEconomizer.differential_dry_bulb,
                    night_cycle=NightCycle.cycle_on_control_zone,
                    heat_recovery=HeatRecovery.enthalpy,
                    heat_recovery_exchanger=HeatRecoveryExchanger.rotary,
                ),
            ]
        )

        # Manual ventilation each 20 minutes for 4 ACH and with auto-calculated ACH
        for flow_rate_in_ach, windows_opening_ratio, variant_name in (
            (4, None, "D_W4y_covid"),
            (None, 1.0, "D_Way_covid"),
            (None, 0.5, "D_Wa.50y_covid"),
            (None, 0.25, "D_Wa.25y_covid"),
        ):
            cls.create_variant(
                [
                    Heating(type=Heating.Type.district),
                    NaturalVentilation(
                        type=NaturalVentilation.Type.windows,
                        flow_rate_in_ach=flow_rate_in_ach,
                        windows_opening_ratio=windows_opening_ratio,
                        time_ranges={
                            NaturalVentilation.Period.winter: DynamicRange(20, 5),
                            NaturalVentilation.Period.summer: DynamicRange(20, 10),
                        },
                    ),
                ],
                variant_name=variant_name,
                number_of_people_fraction=0.5,
            )

        if DEBUG:
            debug(f"Generated {cls.count} scenario variants")


def list_variants_in_module_scope():
    return [
        name
        for name, obj in inspect.getmembers(module)
        if inspect.isclass(obj) and obj is not Scenario and issubclass(obj, Scenario)
    ]


__all__ = list_variants_in_module_scope()
if not __all__:
    Generator.generate_interesting_scenarios()

    # You can also subclass the Scenario model here to add your own variants in addition
    # to the auto-generated ones.

    __all__ = list_variants_in_module_scope()

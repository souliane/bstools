# -*- coding: utf-8 -*-
from ..lib.tools import TooLessObjects, get  # @UnusedImport


class NoZoneListDefinition(Exception):
    pass


class Editor:
    def __init__(self):
        self.load()

    @classmethod
    def get(cls, objs: list, attr: str = None, value: str = None):
        if attr and value is not None:
            objs = [obj for obj in objs if getattr(obj, attr) == value]

        return get(objs)

    def load(self):
        raise NotImplemented

    def save(self):
        raise NotImplemented

    def set_attribute(self, obj_type, attribute, value_pattern):
        raise NotImplemented

    def get_zone_names(self, space_types: list) -> list:
        """
        :raises ObjectTypeNotInModel
        """
        raise NotImplemented

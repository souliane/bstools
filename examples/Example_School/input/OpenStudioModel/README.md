The school model from `Grein.osm` and `in.idf` is the work of Alexandra Heiderer.
It was originally an IDF file that has been partly converted to an OSM file, but
all the geometry and most of the configuration is from her.

Many thanks!
# insert your copyright here

# see the URL below for information on how to write OpenStudio measures
# http://nrel.github.io/OpenStudio-user-documentation/reference/measure_writing_guide/

# start the measure
class FixGreinSchoolModel < OpenStudio::Measure::EnergyPlusMeasure
  # human readable name
  def name
    # Measure name should be the title case of the class name.
    return 'FixGreinSchoolModel'
  end

  # human readable description
  def description
    return ''
  end

  # human readable description of modeling approach
  def modeler_description
    return ''
  end

  # define the arguments that the user will input
  def arguments(workspace)
    args = OpenStudio::Measure::OSArgumentVector.new
    return args
  end

  def remove_objects(workspace, key)
    objects = workspace.getObjectsByType(key.to_IddObjectType)
    objects.each do |object|
      object.remove
    end
  end

  # define what happens when the measure is run
  def run(workspace, runner, user_arguments)
    super(workspace, runner, user_arguments)
    remove_objects(workspace, "OutputControl:Table:Style")
	remove_objects(workspace, "SizingPeriod:DesignDay")
	remove_objects(workspace, "ZoneAirContaminantBalance")
    return true
  end
end

# register the measure to be used by the application
FixGreinSchoolModel.new.registerWithApplication

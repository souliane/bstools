Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	0,53	?
63 Hz	0,61	?
80 Hz	0,63	 
100 Hz	0,71	 
125 Hz	0,55	 
160 Hz	0,70	 
200 Hz	0,70	 
250 Hz	0,84	 
315 Hz	0,90	 
400 Hz	0,84	 
500 Hz	0,90	 
630 Hz	0,94	 
800 Hz	0,93	 
1 kHz	0,88	 
1.25 kHz	0,87	 
1.6 kHz	0,87	 
2 kHz	0,88	 
2.5 kHz	0,79	 
3.15 kHz	0,85	 
4 kHz	0,82	 
5 kHz	0,77	 
6.3 kHz	0,70	 
8 kHz	0,59	 
10 kHz	0,47	 

T20 A	0,88	 
T20 Z	0,89	 

T30 A	0,89	 
T30 Z	0,89	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	2,76	?
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	0,56	 
125 Hz	0,57	 
160 Hz	0,75	 
200 Hz	0,71	 
250 Hz	0,74	 
315 Hz	0,83	 
400 Hz	0,81	 
500 Hz	0,88	 
630 Hz	0,90	 
800 Hz	0,96	 
1 kHz	0,89	 
1.25 kHz	0,88	 
1.6 kHz	0,88	 
2 kHz	0,85	 
2.5 kHz	0,82	 
3.15 kHz	0,88	 
4 kHz	0,87	 
5 kHz	0,79	 
6.3 kHz	0,71	 
8 kHz	0,61	 
10 kHz	0,48	 

T20 A	0,88	 
T20 Z	0,89	 

T30 A	0,89	 
T30 Z	0,89	 

Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	1,55	 
100 Hz	0,91	 
125 Hz	0,88	 
160 Hz	0,95	 
200 Hz	0,77	 
250 Hz	0,83	 
315 Hz	0,81	 
400 Hz	1,04	 
500 Hz	1,01	 
630 Hz	0,86	 
800 Hz	0,77	 
1 kHz	0,87	 
1.25 kHz	0,90	 
1.6 kHz	0,90	 
2 kHz	0,94	 
2.5 kHz	0,85	 
3.15 kHz	0,79	 
4 kHz	0,77	 
5 kHz	0,75	 
6.3 kHz	0,70	 
8 kHz	0,57	 
10 kHz	0,48	 

T20 A	0,88	 
T20 Z	0,91	 

T30 A	0,87	 
T30 Z	0,91	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	2,05	 
100 Hz	0,99	 
125 Hz	0,88	 
160 Hz	0,98	 
200 Hz	0,70	 
250 Hz	0,82	 
315 Hz	0,89	 
400 Hz	0,93	 
500 Hz	0,89	 
630 Hz	0,86	 
800 Hz	0,83	 
1 kHz	0,82	 
1.25 kHz	0,89	 
1.6 kHz	0,90	 
2 kHz	0,91	 
2.5 kHz	0,88	 
3.15 kHz	0,85	 
4 kHz	0,76	 
5 kHz	0,72	 
6.3 kHz	0,69	 
8 kHz	0,59	 
10 kHz	0,49	 

T20 A	0,88	 
T20 Z	0,91	 

T30 A	0,87	 
T30 Z	0,91	 

Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	0,81	 
80 Hz	0,70	 
100 Hz	0,78	 
125 Hz	0,79	 
160 Hz	1,04	 
200 Hz	1,37	 
250 Hz	1,18	 
315 Hz	1,32	 
400 Hz	1,31	 
500 Hz	1,42	 
630 Hz	1,66	 
800 Hz	1,70	 
1 kHz	1,47	 
1.25 kHz	1,45	 
1.6 kHz	1,43	 
2 kHz	1,52	 
2.5 kHz	1,41	 
3.15 kHz	1,14	 
4 kHz	1,06	 
5 kHz	0,96	 
6.3 kHz	0,78	 
8 kHz	0,68	 
10 kHz	0,57	 

T20 A	1,53	 
T20 Z	1,50	 

T30 A	1,64	 
T30 Z	1,50	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	1,31	?
80 Hz	0,68	 
100 Hz	1,36	?
125 Hz	1,15	 
160 Hz	1,50	 
200 Hz	1,40	 
250 Hz	1,36	 
315 Hz	1,55	 
400 Hz	1,50	 
500 Hz	1,58	 
630 Hz	1,72	 
800 Hz	1,72	 
1 kHz	1,59	 
1.25 kHz	1,62	 
1.6 kHz	1,61	 
2 kHz	1,60	 
2.5 kHz	1,46	 
3.15 kHz	1,26	 
4 kHz	1,12	 
5 kHz	1,04	 
6.3 kHz	0,85	 
8 kHz	0,71	 
10 kHz	0,58	 

T20 A	1,53	 
T20 Z	1,50	 

T30 A	1,64	 
T30 Z	1,50	 

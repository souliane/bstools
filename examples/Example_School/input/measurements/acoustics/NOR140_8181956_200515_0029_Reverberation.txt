Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	0,42	?
63 Hz	0,31	?
80 Hz	0,34	?
100 Hz	0,74	 
125 Hz	0,86	 
160 Hz	0,78	 
200 Hz	0,80	 
250 Hz	0,85	 
315 Hz	0,72	 
400 Hz	0,75	 
500 Hz	0,79	 
630 Hz	0,81	 
800 Hz	0,87	 
1 kHz	0,89	 
1.25 kHz	0,78	 
1.6 kHz	0,90	 
2 kHz	0,82	 
2.5 kHz	0,83	 
3.15 kHz	0,84	 
4 kHz	0,81	 
5 kHz	0,76	 
6.3 kHz	0,68	 
8 kHz	0,57	 
10 kHz	0,48	 

T20 A	0,84	 
T20 Z	0,83	 

T30 A	0,86	 
T30 Z	0,83	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	0,75	?
63 Hz	N/A	 
80 Hz	0,39	?
100 Hz	0,59	 
125 Hz	0,71	 
160 Hz	0,71	 
200 Hz	0,60	 
250 Hz	0,78	 
315 Hz	0,87	 
400 Hz	0,77	 
500 Hz	0,82	 
630 Hz	0,82	 
800 Hz	0,93	 
1 kHz	0,90	 
1.25 kHz	0,87	 
1.6 kHz	0,86	 
2 kHz	0,84	 
2.5 kHz	0,86	 
3.15 kHz	0,86	 
4 kHz	0,83	 
5 kHz	0,78	 
6.3 kHz	0,70	 
8 kHz	0,57	 
10 kHz	0,48	 

T20 A	0,84	 
T20 Z	0,83	 

T30 A	0,86	 
T30 Z	0,83	 

Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	0,61	?
63 Hz	N/A	 
80 Hz	0,39	?
100 Hz	0,35	?
125 Hz	0,61	 
160 Hz	0,56	 
200 Hz	0,84	 
250 Hz	0,72	 
315 Hz	0,86	 
400 Hz	0,83	 
500 Hz	0,84	 
630 Hz	0,94	 
800 Hz	0,90	 
1 kHz	0,95	 
1.25 kHz	0,89	 
1.6 kHz	0,90	 
2 kHz	0,94	 
2.5 kHz	0,88	 
3.15 kHz	0,87	 
4 kHz	0,89	 
5 kHz	0,80	 
6.3 kHz	0,69	 
8 kHz	0,60	 
10 kHz	0,51	 

T20 A	0,91	 
T20 Z	0,90	 

T30 A	0,91	 
T30 Z	0,90	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	0,74	 
125 Hz	1,66	 
160 Hz	0,61	 
200 Hz	0,85	 
250 Hz	0,75	 
315 Hz	0,77	 
400 Hz	0,82	 
500 Hz	0,84	 
630 Hz	0,94	 
800 Hz	0,96	 
1 kHz	0,93	 
1.25 kHz	0,82	 
1.6 kHz	0,88	 
2 kHz	0,94	 
2.5 kHz	0,87	 
3.15 kHz	0,91	 
4 kHz	0,89	 
5 kHz	0,80	 
6.3 kHz	0,72	 
8 kHz	0,59	 
10 kHz	0,51	 

T20 A	0,91	 
T20 Z	0,90	 

T30 A	0,91	 
T30 Z	0,90	 

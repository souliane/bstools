Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	0,28	?
125 Hz	0,67	 
160 Hz	0,51	 
200 Hz	0,84	 
250 Hz	0,91	 
315 Hz	0,81	 
400 Hz	0,83	 
500 Hz	0,84	 
630 Hz	0,79	 
800 Hz	0,90	 
1 kHz	0,88	 
1.25 kHz	0,95	 
1.6 kHz	0,90	 
2 kHz	0,94	 
2.5 kHz	0,95	 
3.15 kHz	0,92	 
4 kHz	0,90	 
5 kHz	0,79	 
6.3 kHz	0,77	 
8 kHz	0,62	 
10 kHz	0,49	 

T20 A	0,88	 
T20 Z	0,86	 

T30 A	0,90	 
T30 Z	0,86	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	N/A	 
125 Hz	0,97	 
160 Hz	0,78	 
200 Hz	0,62	?
250 Hz	0,85	 
315 Hz	0,76	 
400 Hz	0,81	 
500 Hz	0,88	 
630 Hz	0,83	 
800 Hz	0,94	 
1 kHz	0,93	 
1.25 kHz	0,91	 
1.6 kHz	0,86	 
2 kHz	0,88	 
2.5 kHz	0,90	 
3.15 kHz	0,91	 
4 kHz	0,89	 
5 kHz	0,81	 
6.3 kHz	0,73	 
8 kHz	0,62	 
10 kHz	0,49	 

T20 A	0,88	 
T20 Z	0,86	 

T30 A	0,90	 
T30 Z	0,86	 

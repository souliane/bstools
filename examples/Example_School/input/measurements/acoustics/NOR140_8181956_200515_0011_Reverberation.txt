Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	1,33	 
125 Hz	0,70	 
160 Hz	0,59	 
200 Hz	0,82	 
250 Hz	0,86	 
315 Hz	0,81	 
400 Hz	0,88	 
500 Hz	0,83	 
630 Hz	0,99	 
800 Hz	1,04	 
1 kHz	0,94	 
1.25 kHz	1,01	 
1.6 kHz	0,96	 
2 kHz	0,93	 
2.5 kHz	0,87	 
3.15 kHz	0,85	 
4 kHz	0,76	 
5 kHz	0,74	 
6.3 kHz	0,72	 
8 kHz	0,61	 
10 kHz	0,52	 

T20 A	0,97	 
T20 Z	0,96	 

T30 A	0,94	 
T30 Z	0,96	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	N/A	 
125 Hz	0,90	 
160 Hz	0,81	 
200 Hz	0,93	 
250 Hz	0,86	 
315 Hz	0,98	 
400 Hz	0,89	 
500 Hz	0,93	 
630 Hz	0,91	 
800 Hz	0,91	 
1 kHz	0,98	 
1.25 kHz	0,99	 
1.6 kHz	0,91	 
2 kHz	0,96	 
2.5 kHz	0,92	 
3.15 kHz	0,88	 
4 kHz	0,80	 
5 kHz	0,77	 
6.3 kHz	0,72	 
8 kHz	0,62	 
10 kHz	0,50	 

T20 A	0,97	 
T20 Z	0,96	 

T30 A	0,94	 
T30 Z	0,96	 

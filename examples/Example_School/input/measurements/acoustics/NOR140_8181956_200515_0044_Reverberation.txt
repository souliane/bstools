Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	0,89	 
100 Hz	1,17	 
125 Hz	0,99	 
160 Hz	0,76	 
200 Hz	0,90	 
250 Hz	1,10	 
315 Hz	1,15	 
400 Hz	0,99	 
500 Hz	0,85	 
630 Hz	0,91	 
800 Hz	0,90	 
1 kHz	0,92	 
1.25 kHz	0,91	 
1.6 kHz	0,80	 
2 kHz	0,79	 
2.5 kHz	0,75	 
3.15 kHz	0,76	 
4 kHz	0,79	 
5 kHz	0,72	 
6.3 kHz	0,70	 
8 kHz	0,58	 
10 kHz	0,47	 

T20 A	0,88	 
T20 Z	0,90	 

T30 A	0,90	 
T30 Z	0,90	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	1,71	 
100 Hz	1,39	 
125 Hz	1,36	 
160 Hz	0,90	 
200 Hz	0,84	 
250 Hz	1,00	 
315 Hz	1,08	 
400 Hz	0,89	 
500 Hz	0,90	 
630 Hz	0,92	 
800 Hz	0,89	 
1 kHz	0,90	 
1.25 kHz	0,90	 
1.6 kHz	0,87	 
2 kHz	0,84	 
2.5 kHz	0,82	 
3.15 kHz	0,74	 
4 kHz	0,75	 
5 kHz	0,71	 
6.3 kHz	0,67	 
8 kHz	0,58	 
10 kHz	0,48	 

T20 A	0,88	 
T20 Z	0,90	 

T30 A	0,90	 
T30 Z	0,90	 

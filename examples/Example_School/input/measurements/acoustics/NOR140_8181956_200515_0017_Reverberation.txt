Primary reverberation time - 	T20
Frequency (Hz)	T20 mean (sec)	Status	T20 stdev (sec)
50 Hz	1,14	 
63 Hz	N/A	 
80 Hz	4,69	?
100 Hz	4,25	 
125 Hz	0,98	 
160 Hz	1,13	 
200 Hz	1,51	 
250 Hz	1,25	 
315 Hz	1,68	 
400 Hz	1,57	 
500 Hz	1,92	 
630 Hz	1,86	 
800 Hz	1,57	 
1 kHz	1,89	 
1.25 kHz	1,90	 
1.6 kHz	1,76	 
2 kHz	1,70	 
2.5 kHz	1,54	 
3.15 kHz	1,37	 
4 kHz	1,28	 
5 kHz	1,16	 
6.3 kHz	1,02	 
8 kHz	0,81	 
10 kHz	0,64	 

T20 A	1,76	 
T20 Z	1,75	 

T30 A	1,81	 
T30 Z	1,75	 

Secondary reverberation time - 	T30
Frequency (Hz)	T30 mean (sec)	Status	T30 stdev (sec)
50 Hz	N/A	 
63 Hz	N/A	 
80 Hz	N/A	 
100 Hz	N/A	 
125 Hz	1,57	 
160 Hz	1,33	 
200 Hz	1,37	 
250 Hz	1,22	 
315 Hz	1,74	 
400 Hz	1,59	 
500 Hz	1,89	 
630 Hz	1,84	 
800 Hz	1,83	 
1 kHz	1,85	 
1.25 kHz	1,91	 
1.6 kHz	1,74	 
2 kHz	1,72	 
2.5 kHz	1,58	 
3.15 kHz	1,37	 
4 kHz	1,27	 
5 kHz	1,14	 
6.3 kHz	1,01	 
8 kHz	0,84	 
10 kHz	0,64	 

T20 A	1,76	 
T20 Z	1,75	 

T30 A	1,81	 
T30 Z	1,75	 

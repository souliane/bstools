import filecmp

import pytest

from bstools.plotlib.image import PNGImage


class TimeSeriesProcessorTestMixin:
    processor_cls = NotImplemented
    processor_extra_kwargs = {}
    date_range = ("2020-01-01", "2020-01-31")

    @pytest.fixture
    def feather_paths(self):
        raise NotImplementedError

    @pytest.fixture
    def pre_delete_feather_dumps(self, feather_paths):
        for path in feather_paths:
            if path.exists():
                path.unlink()

        yield

    @pytest.fixture
    def processor(self):
        return self.processor_cls(
            date_range=self.date_range,
            ignore_existing_dumps=False,
            overwrite=True,
            **self.processor_extra_kwargs,
        )

    @pytest.fixture
    def processor_ignoring_existing_dumps(self):
        return self.processor_cls(
            date_range=self.date_range,
            ignore_existing_dumps=True,
            overwrite=True,
            **self.processor_extra_kwargs,
        )

    @pytest.fixture
    def input_filepaths(self, processor):
        return processor.get_input_filepaths()

    @pytest.fixture
    def dataframe(self, processor, input_filepaths):
        return processor.parse_input_files(input_filepaths)

    def assert_similar_plots_directories(self, dir_1: str, dir_2: str):
        result = filecmp.dircmp(dir_1, dir_2)

        assert not any((result.left_only, result.right_only))
        for filename in result.diff_files:
            assert PNGImage.equal(f"{dir_1}/{filename}", f"{dir_2}/{filename}")

    def test_parse_input_files_from_scratch(
        self,
        dataframe,
        input_filepaths,
        pre_delete_feather_dumps,
        processor_ignoring_existing_dumps,
    ):
        new_df = processor_ignoring_existing_dumps.parse_input_files(input_filepaths)

        # `new_df` might contain an extra row for 2020-01-31 00:00
        assert len(dataframe.merge(new_df)) == len(dataframe)

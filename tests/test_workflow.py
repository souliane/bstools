import filecmp
from pathlib import Path

import mock
import pytest

from bstools.epluslib import variants
from bstools.lib.os import OSTools
from bstools.settings import Settings

variants.__all__


class TestWorkflow:
    def _build_variant_result_path(self, path: Path, variant_name: str) -> Path:
        return path.parent / (path.stem + f".{variant_name}" + path.suffix)

    def _build_variant_golden_path(self, variant_result_path: Path) -> Path:
        return Path("tests/assets").joinpath(*variant_result_path.parts[-3:])

    @pytest.fixture
    def pre_delete_workflow_results(self):
        path = Path("examples/Example_School/output/results/D")
        if path.exists():
            OSTools.remove_directory(path)
        yield

    @pytest.mark.parametrize("variant_name", variants.__all__)
    def test_edit_model(self, variant_name: str, workflow):
        workflow.run(variant_name=variant_name, skip_simulation=True, overwrite=True)

        idf_path = Settings.Energyplus.idf_path
        expanded_idf_path = idf_path.parent / "expanded.idf"

        for path in (idf_path, expanded_idf_path):
            result_path = self._build_variant_result_path(path, variant_name)
            expected_path = self._build_variant_golden_path(result_path)

            # test that the generated file has been copied
            assert filecmp.cmp(path, result_path)

            # test that the copied file has the expected content
            assert filecmp.cmp(result_path, expected_path)

            # make also sure the test above did not compare the same files...
            assert result_path != expected_path

    def test_workflow(self, pre_delete_workflow_results, workflow):
        with mock.patch("bstools.epluslib.scenario.DEBUG", True):
            workflow.run(
                "D",
                overwrite=True,
                skip_simulation=False,
                use_existing_results=False,
            )

        # we need to remove a timestamp from e+ results on lines 1 and 179
        eplus_result = "examples/Example_School/output/results/D/eplustbl.csv"
        eplus_result_fixed = eplus_result + ".without_timestamps"
        with open(eplus_result, "r") as fp, open(eplus_result_fixed, "w") as fp_fixed:
            for index, line in enumerate(fp.read().splitlines()):
                fp_fixed.write(
                    (line[: line.find("YMD=") + 4] if index in (0, 178) else line)
                    + "\r\n"
                )

        # check energyplus results
        assert filecmp.cmp(
            eplus_result_fixed,
            "tests/assets/output/results/D_debug/eplustbl.csv",
        )

        # check openstudio results
        assert filecmp.cmp(
            "examples/Example_School/output/results/D/results.json",
            "tests/assets/output/results/D_debug/results.json",
        )

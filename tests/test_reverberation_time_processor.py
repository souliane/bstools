import filecmp

from bstools.plotlib.image import PNGImage


class TestReverberationTimeProcessor:
    def test_process(self, reverberation_time_processor):
        reverberation_time_processor.process()

        directories = (
            "examples/Example_School/output/plots/acoustics",
            "tests/assets/output/plots/acoustics",
        )
        result = filecmp.dircmp(*directories)

        assert not any((result.left_only, result.right_only))
        for filename in result.diff_files:
            assert PNGImage.equal(
                *(f"{directory}/{filename}" for directory in directories)
            )

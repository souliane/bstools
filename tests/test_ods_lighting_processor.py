import filecmp

from bstools.plotlib.image import PNGImage


class TestODSLightingProcessor:
    def test_process(self, ods_lighting_processor):
        ods_lighting_processor.process()

        directories = (
            "examples/Example_School/output/plots/lighting",
            "tests/assets/output/plots/lighting",
        )
        result = filecmp.dircmp(*directories)

        assert not any((result.left_only, result.right_only))
        for filename in result.diff_files:
            assert PNGImage.equal(
                *(f"{directory}/{filename}" for directory in directories)
            )

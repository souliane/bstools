class TestIDFEditor:
    def test_search_idd(self, idf_editor, capfd):
        idf_editor.search_idd("Night Ventilation")

        out, _ = capfd.readouterr()

        assert (
            out
            == """Searching for 'Night Ventilation':
Fan:SystemModel
  Night Ventilation Mode Pressure Rise
  Night Ventilation Mode Flow Fraction
FanPerformance:NightVentilation
AvailabilityManager:NightVentilation
  Ventilation Temperature Schedule Name
  Ventilation Temperature Low Limit
  Night Venting Flow Fraction
  Control Zone Name
"""
        )

    def test_search_idd_verbose(self, idf_editor, capfd):
        idf_editor.search_idd("Night Ventilation Mode Pressure Rise", verbose=True)

        out, _ = capfd.readouterr()

        assert (
            out
            == """Searching for 'Night Ventilation Mode Pressure Rise':

Fan:SystemModel
  Versatile simple fan that can be used in variable air volume, constant volume, on-off cycling, two-speed or multi-speed applications.
  Performance at different flow rates, or speed levels, is determined using separate performance curve or table
  or prescribed power fractions at discrete speed levels for two-speed or multi-speed fans.

  Night Ventilation Mode Pressure Rise
    Total system fan pressure rise at the fan when in night mode using AvailabilityManager:NightVentilation
"""
        )

from pathlib import Path

import pytest

from bstools.epluslib.edit_idf import IDFEditor
from bstools.epluslib.hvac_template import HVACTemplateExpander
from bstools.epluslib.workflow import Workflow
from bstools.plots.acoustics import ReverberationTimeProcessor
from bstools.plots.lighting import ODSLightingProcessor


@pytest.fixture(scope="class")
def workflow():
    return Workflow()


@pytest.fixture(scope="class")
def idf_editor():
    return IDFEditor(idf_path=Path("dummy.idf"))


@pytest.fixture(scope="class")
def hvac_template_expander():
    return HVACTemplateExpander()


@pytest.fixture(scope="class")
def reverberation_time_processor():
    return ReverberationTimeProcessor(overwrite=True)


@pytest.fixture(scope="class")
def ods_lighting_processor():
    return ODSLightingProcessor(overwrite=True)

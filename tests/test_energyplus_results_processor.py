from pathlib import Path
from unittest.mock import patch

import pytest

from bstools.plots.energyplus import EnergyplusResultsProcessor
from bstools.settings import Settings
from tests.test_time_series_processor import TimeSeriesProcessorTestMixin


class TestEnergyplusResultsProcessor(TimeSeriesProcessorTestMixin):
    processor_cls = EnergyplusResultsProcessor
    processor_extra_kwargs = {"dirname": "Natural"}

    @pytest.fixture
    def feather_paths(self):
        return [
            # use D_debug because the results for full year simulation are too large
            Path(f"tests/assets/output/results/D_debug/eplusout.csv.feather")
        ]

    @pytest.fixture
    def input_filepaths(self, processor):
        # use D_debug because the results for full year simulation are too large
        return [Path("tests/assets/output/results/D_debug/eplusout.csv")]

    def test_process_groups(self):
        with patch.object(
            EnergyplusResultsProcessor.settings,
            "input_dir",
            Path("tests/assets/output/results"),
        ):
            EnergyplusResultsProcessor.process_groups(
                init_kwargs={"overwrite": True},
                group=Settings.EnergyplusResultsProcessor.Group(
                    "Natural", ("D", "D_W2y", "D_W4y")
                ),
            )

        self.assert_similar_plots_directories(
            "examples/Example_School/output/plots/energyplus/Jan/Natural",
            "tests/assets/output/plots/energyplus/Jan/Natural",
        )

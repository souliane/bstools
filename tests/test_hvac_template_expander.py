import filecmp

from pyexcel_ods import get_data as ods_get_data


class TestHVACTemplateExpander:
    def test_expand_hvac_template(self, hvac_template_expander):
        hvac_template_expander.run(
            search_objects=None, diff_templates=None, overwrite=True
        )

        for filepath in ("HVACTemplate.json", "HVACTemplate.csv"):
            assert filecmp.cmp(filepath, f"tests/assets/{filepath}")

        assert ods_get_data("HVACTemplate.ods") == ods_get_data(
            "tests/assets/HVACTemplate.ods"
        )

    def test_search_hvac_template(self, hvac_template_expander, capfd):
        hvac_template_expander.run(
            search_objects="Fan:VariableVolume,AvailabilityManager:NightCycle",
            diff_templates=None,
            overwrite=False,
        )

        out, _ = capfd.readouterr()

        assert (
            out
            == """HVACTemplates that expand to ['AvailabilityManager:NightCycle', 'Fan:VariableVolume']: ['System:PackagedVAV', 'System:VAV']
"""
        )

    def test_diff_hvac_template(self, hvac_template_expander, capfd):
        hvac_template_expander.run(
            search_objects=None,
            diff_templates="System:PackagedVAV,System:VAV",
            overwrite=False,
        )
        out, _ = capfd.readouterr()

        assert (
            out
            == """--- System:PackagedVAV
+++ System:VAV
-Coil:Cooling:DX:TwoSpeed
-CoilSystem:Cooling:DX
+Coil:Cooling:Water
-Curve:Biquadratic
-Curve:Cubic
-Curve:Quadratic
+Controller:WaterCoil
"""
        )

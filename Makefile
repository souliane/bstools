.DEFAULT_GOAL := help
.PHONY: tests ptw

DOCKER_IMAGE = souliane/bstools:0.1
RUN = docker run -it -v `pwd`:/bstools $(DOCKER_IMAGE)
TEST_ARGS = tests --maxfail=1 --failed-first -s

OPENSTUDIO = https://github.com/NREL/OpenStudio/releases/download/v3.1.0/OpenStudio-3.1.0+e165090621-Linux.deb

docker_build:  ## Build Docker image
	docker image build . -t $(DOCKER_IMAGE)

docker_bash:  # Run Docker image 
	$(RUN) $(ARGS)

requirements:  ## Install requirements
	pip install -r requirements.txt -r requirements.txt $(ARGS) 

formatting:  ## Auto-formatting with black and isort
	black src; isort src

tests:  ## Run tests
	PROJECTPATH=examples/Example_School pytest $(TEST_ARGS) $(ARGS)

ptw:  ## Run tests with pytest-watch
	PROJECTPATH=examples/Example_School/ ptw src tests -- $(TEST_ARGS) $(ARGS)
